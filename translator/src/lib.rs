//! Library for converting tree mappings into linear-indexed state data
//! nyaa.
use std::error::Error;
use std::hash::{Hash, BuildHasher};
use std::collections::{hash_map::{HashMap, RandomState}, hash_set::HashSet};

/// The type used for indices into translation tables nya
type TransIdx = usize;

/// Holds a bidirectional mapping with allocated, sequential, indices for 
/// each value of the given type nya
#[derive(Clone, Debug)]
pub struct TranslationTable<T: Hash + Clone + Eq, RG = RandomState> {
    /// Mapping from the item to the associated linear state/index nya
    to_linear_states: HashMap::<T, TransIdx, RG>,
    /// Mapping from the linear state/index back to the item nya
    from_linear_states: Vec::<T>
}


impl <T: Hash + Clone + Eq, RG: Default + BuildHasher> TranslationTable::<T, RG> {
    pub fn from_vec(data_in_states: Vec::<T>) -> Self {
        data_in_states.into()
    }

    pub fn new(states: HashSet::<T, RG>) -> Self {
        states.into()
    }    
}

impl <T: Hash + Clone + Eq, RG: BuildHasher> TranslationTable::<T, RG> {
    pub fn get_linear_state(&self, state: &T) -> Option::<TransIdx>{
        self.to_linear_states.get(state).copied()
    }
}

impl <T: Hash + Clone + Eq, RG> TranslationTable::<T, RG> {


    /// Get an iterator over the states in the order of their
    /// linear-state mappings nya.
    pub fn iter_over_states(&self) -> impl Iterator::<Item=&'_ T> {
        self.from_linear_states.iter()
    }

    pub fn get_from_linear_state(&self, linstate: TransIdx) -> Option<&T> {
        self.from_linear_states.get(linstate)
    }


    /// Get the vec map from linear/index states to original states nya
    pub fn linear_state_vec(&self) -> &Vec::<T> {
        &self.from_linear_states
    }

    pub fn state_to_linear_map(&self) -> &HashMap::<T, TransIdx, RG> {
        &self.to_linear_states
    }

    /// Number of linear states/states in this translation table nya
    pub fn len(&self) -> usize {
        self.from_linear_states.len()
    }

    /// Translate a map into a vector using this translation table nya
    /// (where the indices in the output correspond to the linearised
    /// states of this translation table nya).
    ///
    /// States in the map lacking a translation are spat out in the returned
    /// hashmap.
    ///
    /// Error is produced if there are states in the translation table that lack
    /// a value
    pub fn raw_translate<RG1: BuildHasher, V>(&self, trans_map: HashMap::<T, V, RG1>) 
        -> Result::<(Vec::<V>, HashMap::<T, V, RG1>), NoTranslatedValErr::<T>> { 
        let (r, leftovers): (Result::<Vec::<_>, _>, _) = self.default_translate_map(
            trans_map,
            |_, value| Ok(value),
            |state| Err(NoTranslatedValErr::new(state.clone()))
        );

        Ok((r?, leftovers)) 
    }

    /// Maybe-translate a map.
    /// spits out a vector of optional(value)s along with leftovers.
    /// The optionals are None if there is no value for a given translation index nya
    pub fn maybe_translate<RG1: BuildHasher, V>(&self, trans_map: HashMap::<T, V, RG1>)
        -> (Vec::<Option::<V>>, HashMap::<T, V, RG1>) {
        let (maybetranses, leftovers): (Vec::<_>, _) = self.default_translate_map(
            trans_map,
            |_, value| Some(value),
            |_| None
        );
        (maybetranses, leftovers)
    }

    /// Translate a map into a container using this translation table,
    /// including with a transform, and default function for when the 
    /// translation table does not have a translation for the given
    /// key in the map nya
    ///
    /// This essentially is like setting a value of Container to an iterator
    /// over the values in linear state space in order (or for states lacking a
    /// value, the return of no_translation_transform) nya
    pub fn default_translate_map<
        RG1: BuildHasher, 
        V, 
        VTrans, 
        Container: std::iter::FromIterator::<VTrans>
    > (
        &self,
        mut to_translate: HashMap::<T, V, RG1>,
        mut has_translation_transform: impl FnMut(&T, V) -> VTrans,
        mut no_translation_transform: impl FnMut(&T) -> VTrans
    ) -> (Container, HashMap::<T, V, RG1>) {
        let linspace_map: Container = self.iter_over_states()
            .map(|state| to_translate
                .remove_entry(state)
                .map_or_else(
                    || no_translation_transform(state),
                    |(_k, v)| has_translation_transform(state, v)
                )
            ).collect();
        (linspace_map, to_translate)
    }
}

impl <T: Hash + Clone + Eq, RG: Default> std::ops::Index::<TransIdx> 
for TranslationTable::<T, RG> {
    type Output = T;

    fn index(&self, index: TransIdx) -> &Self::Output {
        &self.from_linear_states[index]
    }
}



impl <T: Hash + Clone + Eq, RG: BuildHasher + Default> From::<HashSet::<T, RG>> 
for TranslationTable::<T, RG> {
    fn from(data: HashSet::<T, RG>) -> Self {
        let from_linear_states: Vec::<_> = data.into_iter().collect();
        let to_linear_states: HashMap::<_, _, RG> = from_linear_states.iter()
            .enumerate()
            .map(|(idx, item)| (item.clone(), idx))
            .collect();
        Self {
            to_linear_states,
            from_linear_states
        }
    }
}


impl <T: Hash + Clone + Eq, RG: Default + BuildHasher> From::<Vec::<T>> 
for TranslationTable::<T, RG> {
    fn from(data: Vec::<T>) -> Self {
        // Note that we cannot assume non-duplication here... we have to 
        // dedup the vector first nyaa
        // We do cleverness by using the hashmap also like a hashset to do filtering
        // of duplicates.
        //
        // Cannot use iterators for this because closures grab ownership not on 
        // running but when they are made nya, and filtering and mapping 
        // causes issues here nya
        //
        // Note that we shouldn't capacity() for any vector/map because we do not
        // know the level of duplication in input here nya
        let mut to_linear_states = HashMap::<T, TransIdx, RG>::with_hasher(RG::default());
        let mut from_linear_states = Vec::<T>::new();
        let mut filtered_idx = 0usize;
        for state in data {
            if !to_linear_states.contains_key(&state) {
                to_linear_states.insert(state.clone(), filtered_idx);
                from_linear_states.push(state);
                filtered_idx += 1;
            }
        }
        Self {
            to_linear_states,
            from_linear_states
        } 
    }
}

/// Holds a map via a translation table combined with linear memory indices
/// holding actual items nya.
///
/// the idea is to make bulk indexing more efficient by generating states
/// in the form of linear indices nya, and translating the original mapped
/// items into these in bulk so you can then use them as linear memory indices 
/// rather than having to go through the nonlinear structure inside a hashmap nya
///
/// Note that these are immutable data structures and to mutate the map you will have
/// to delinearise it ;3
#[derive(Clone, Debug)]
pub struct LinearisedMap <K: Hash + Clone + Eq, V, RG> {
    /// Translation table: (key <-> linear state)
    translation_table: TranslationTable::<K, RG>,
    /// Linear map (linear state -> value)
    linear_state_map: Vec::<V>
} 

impl <K: Hash + Clone + Eq, V, RG: Default + BuildHasher> LinearisedMap::<K, V, RG> {
    /// Convert a standard hashmap into a linearised map nya
    pub fn linearise(map: HashMap::<K, V, RG>) -> Self {
        // use a specialised translation table constructor here, can be more efficient
        // nyaaa
        let kvpairs: Vec<_> = map.into_iter().collect();
        let mut from_linear_state = Vec::<K>::with_capacity(kvpairs.len());
        let mut linear_state_map = Vec::<V>::with_capacity(kvpairs.len());
        let state_index_map: HashMap::<K, TransIdx, RG> = kvpairs.into_iter()
            .enumerate()
            .map(|(idx, (key, val))| {
                from_linear_state.push(key.clone());
                linear_state_map.push(val);
                (key, idx)
            })
            .collect();
        let trans_table = TranslationTable {
            from_linear_states: from_linear_state,
            to_linear_states: state_index_map
        };

        Self {
            translation_table: trans_table,
            linear_state_map
        }
    }

    pub fn new(map: HashMap::<K, V, RG>) -> Self {
        Self::linearise(map)
    }

    /// Translate a given map with the given table nya
    /// If there is no translation for a key-value then it is ignored
    /// (and returned in the hashmap given as output nya)
    pub fn translate<RG2: BuildHasher>(
        table: TranslationTable::<K, RG>,
        translating_map: HashMap::<K, V, RG2>
    ) -> Result::<
            (Self, HashMap::<K, V, RG2>), 
            NoTranslatedValErr::<K>
        > {
        let res = table.raw_translate(translating_map);
        let (translation, excess) = res?;

        Ok((Self {
            translation_table: table,
            linear_state_map: translation
        }, excess))
    }

    /// Convert the linearised hashmap back into a plain ol' hashmap (of course, 
    /// modifiable nya)
    pub fn delinearise<RG2: Default + BuildHasher>(self) -> HashMap::<K, V, RG2> {
        self.translation_table.from_linear_states.into_iter()
            .zip(self.linear_state_map.into_iter())
            .collect()
    }

    /// Apply a transform to all the values in the current map and return a new 
    /// one nya.
    pub fn transform<T>(self, mut trans: impl FnMut(&TranslationTable::<K, RG>, V) -> T) 
        -> LinearisedMap::<K, T, RG> {
        let (translation_table, data) = (self.translation_table, self.linear_state_map);
        let new_state_map: Vec::<_> =  data.into_iter()
            .map(|d| trans(&translation_table, d))
            .collect();

        LinearisedMap {
            translation_table,
            linear_state_map: new_state_map
        }
    }

    /// Get the translation table for this mapping
    pub fn get_translator(&self) -> &TranslationTable::<K, RG> {
        &self.translation_table
    }

    /// Get the linearised values for this mapping nya
    pub fn get_linearised_values(&self) -> &Vec::<V> {
        &self.linear_state_map
    }

    /// Get the associated linear state nya
    pub fn get_linear_state(&self, key: &K) -> Option::<TransIdx> {
        self.translation_table.get_linear_state(key)
    }

    /// Get the value associated with the given linear state if it exists.
    ///
    /// Get linear states via [`get_linear_state`]
    pub fn get(&self, linear_state: TransIdx) -> Option::<&V> {
        self.linear_state_map.get(linear_state)
    }

    /// Iterate over K, V pairs nyaa
    pub fn get_kv_iter(&self) -> impl Iterator::<Item=(&K, &V)> {
        self.translation_table.iter_over_states()
            .zip((&self.linear_state_map).into_iter())
    }

    /// Iterate over keys only nya
    pub fn get_k_iter(&self) -> impl Iterator::<Item=&K> {
        self.translation_table.iter_over_states()
    }

    /// iterate over linear state and value pairs nya
    pub fn get_linstate_v_iter(&self) -> impl Iterator::<Item=(TransIdx, &V)> {
        self.linear_state_map.iter().enumerate()
    }

    /// Get the number of elements in this linearised map ^.^
    pub fn len(&self) -> usize {
        self.translation_table.len()
    }
}





/// Holds an error for there not being a translation for a given set of states
/// nya
#[derive(Clone, Eq, PartialEq)]
pub struct NoTranslationErr<T: Clone + Hash + Eq, RG: BuildHasher> {
    missing_states: HashSet::<T, RG>
}

impl <T: Hash + Clone + Eq, RG: BuildHasher> NoTranslationErr::<T, RG> {
    fn new(missing_states: HashSet::<T, RG>) -> Self {
        Self {missing_states}
    }

    pub fn missing_states(&self) -> &HashSet::<T, RG> {
        &self.missing_states
    }
}


impl <T: Hash + Clone + Eq, RG: BuildHasher> std::fmt::Display 
for NoTranslationErr::<T, RG> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Missing state in translation table. nya") 
    }
}

impl <T: Hash + Clone + Eq, RG: BuildHasher> std::fmt::Debug 
for NoTranslationErr::<T, RG> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Display>::fmt(self, f)
    }
}

impl <T: Hash + Clone + Eq, RG: BuildHasher> Error for NoTranslationErr::<T, RG> {}

/// Holds an error for there not being a value for a given translation element
/// nyaa
#[derive(Clone)]
pub struct NoTranslatedValErr <T: Clone + Hash + Eq> {
    lacking_map_value: T
}


impl <T: Hash + Clone + Eq> NoTranslatedValErr::<T> {
    fn new(lacking_map_value: T) -> Self {
        Self {lacking_map_value}
    }
}

impl <T: Hash + Clone + Eq> std::fmt::Debug
for NoTranslatedValErr::<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Missing value for the given state to be translated. nya")
    }
}

impl <T: Hash + Clone + Eq> std::fmt::Display 
for NoTranslatedValErr::<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, f)
    }
}

impl <T: Hash + Clone + Eq> Error for NoTranslatedValErr::<T> {}

/// Holds a generalised translation error nyaaa
///
#[derive(Clone)]
pub enum TransErr <T: Hash + Clone + Eq, RG: BuildHasher = RandomState> {
    NoTransFor(NoTranslationErr::<T, RG>),
    NoTransedValFor(NoTranslatedValErr::<T>)
}

impl <T: Hash + Clone + Eq, RG: BuildHasher> From::<NoTranslationErr::<T, RG>> 
for TransErr::<T, RG> {
    fn from(d: NoTranslationErr::<T, RG>) -> Self {
        Self::NoTransFor(d)
    }
}


impl <T: Hash + Clone + Eq, RG: BuildHasher> From::<NoTranslatedValErr::<T>> 
for TransErr::<T, RG> {
    fn from(d: NoTranslatedValErr::<T>) -> Self {
        Self::NoTransedValFor(d)
    }
}


// TODO: enable this when specialisation becomes stable 
/*
impl <T: std::fmt::Display> std::fmt::Display 
for NoTranslationErr::<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Missing state in translation table: {}", &self.missing_state)
    }
}
*/



#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}


/*
translator - library for efficiently translating between Hashed data - maps 
to ordered/sequential data maps and back nya

Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

