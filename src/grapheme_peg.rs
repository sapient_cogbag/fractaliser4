use std::str::FromStr;
use unicode_segmentation;
use unicode_segmentation::UnicodeSegmentation;
use peg::{
    Parse,
    ParseElem,
    ParseLiteral,
    ParseSlice
};
use crate::util::{ConceptualSlice, iter_elem_eq, make_string_chars};
use crate::util;


/// Type for holding a grapheme cluster, private init nya.
///
/// peg grammars do not let the type be parameterised on the input lifetime
/// so this cannot be just a reference since there is no way to grab the lifetime 
/// of that reference nya. It also cannot be unsized since then we can't take slices
/// of it (and taking slices of references just creates the same lifetime issue as before nya)
///
/// Solution:
/// Owning strings but with some stack-allocated chars for small lengths. Most graphemes are 
/// only 1 to 3 codepoints long and as such will be covered by the stack stuff ^.^ nya
///
/// Unfortunately this does mean we lose the easy equality with &str.
///
/// the # of chars in the variants is roughly based on a guesstimate of how much 
/// stack space String will take up on 64 bit systems ;3
#[derive(Clone, Debug)] // TODO: proper hash. Can't just #derive cus need to check proper equality nya
pub(self) enum GraphemeClusterInternal {
    CodePoints0,
    CodePoints1([char; 1]),
    CodePoints2([char; 2]),
    CodePoints3([char; 3]),
    CodePoints4([char; 4]),
    CodePoints5([char; 5]),
    CodePoints6([char; 6]),
    DynCodePoint(String)
}

/// Type for holding a grapheme cluster nya
#[derive(Clone, Debug)]
pub struct GraphemeCluster(GraphemeClusterInternal);

/// Internal character iterator nya 
enum GraphemeCharIterInternal <'a> { 
    CodePoints0(std::iter::Empty::<char>),
    CodePoints1(<&'a[char; 1] as IntoIterator>::IntoIter),
    CodePoints2(<&'a[char; 2] as IntoIterator>::IntoIter),
    CodePoints3(<&'a[char; 3] as IntoIterator>::IntoIter),
    CodePoints4(<&'a[char; 4] as IntoIterator>::IntoIter),
    CodePoints5(<&'a[char; 5] as IntoIterator>::IntoIter),
    CodePoints6(<&'a[char; 6] as IntoIterator>::IntoIter),
    DynCodePoint(std::str::Chars<'a>)
}

/// Holds an iterator over a grapheme cluster's characters nya
pub struct GraphemeClusterIter<'a>(GraphemeCharIterInternal<'a>);

impl <'a> GraphemeClusterIter<'a> {
    pub fn new(data: &'a GraphemeCluster) -> Self {
       Self(match &data.0 {
            GraphemeClusterInternal::CodePoints0 => {
                 GraphemeCharIterInternal::CodePoints0(std::iter::empty())
            },
            GraphemeClusterInternal::CodePoints1(ps) => {
                GraphemeCharIterInternal::CodePoints1(ps.into_iter())
            },
            GraphemeClusterInternal::CodePoints2(ps) => {
                GraphemeCharIterInternal::CodePoints2(ps.into_iter())
            },
            GraphemeClusterInternal::CodePoints3(ps) => {
                GraphemeCharIterInternal::CodePoints3(ps.into_iter())
            },
            GraphemeClusterInternal::CodePoints4(ps) => {
                GraphemeCharIterInternal::CodePoints4(ps.into_iter())
            },
            GraphemeClusterInternal::CodePoints5(ps) => {
                GraphemeCharIterInternal::CodePoints5(ps.into_iter())
            },
            GraphemeClusterInternal::CodePoints6(ps) => {
                GraphemeCharIterInternal::CodePoints6(ps.into_iter())
            },
            GraphemeClusterInternal::DynCodePoint(ps) => {
                GraphemeCharIterInternal::DynCodePoint(ps.as_str().chars())
            }
       })  
    }
}

impl <'a> Iterator for GraphemeClusterIter<'a> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.0 {
            GraphemeCharIterInternal::CodePoints0(ps) => {ps.next()}
            GraphemeCharIterInternal::CodePoints1(ps) => {ps.next().copied()}
            GraphemeCharIterInternal::CodePoints2(ps) => {ps.next().copied()}
            GraphemeCharIterInternal::CodePoints3(ps) => {ps.next().copied()}
            GraphemeCharIterInternal::CodePoints4(ps) => {ps.next().copied()}
            GraphemeCharIterInternal::CodePoints5(ps) => {ps.next().copied()}
            GraphemeCharIterInternal::CodePoints6(ps) => {ps.next().copied()}
            GraphemeCharIterInternal::DynCodePoint(ps) => {ps.next()}
        }
    }
}

/// Make a boolean function that acts on the first
/// character of the grapheme cluster nya
/// Passing another thing after the visibilty and 
/// the name will get it returned as a boolean nya
// https://stackoverflow.com/questions/33999341/generating-documentation-in-macros
macro_rules! make_mc_func{
    ($(#[$attrs:meta])* single $vis:vis fn $name:ident = $from_fn:ident) => {
        $(#[$attrs])*
        #[doc="Forwarding of function on single char, if the item is solely a single
        character."]
        $vis fn $name(&self) -> bool {
            match self.single_codepoint() {
                Some(ch) => ch.$from_fn(),
                None => false
            }
        }
    };
    ($(#[$attrs:meta])* multi $vis:vis fn $name:ident = $from_fn:ident) => {
        $(#[$attrs])*
        #[doc="Forwarding of function on single char, if the first item matches it"]
        $vis fn $name(&self) -> bool {
            match self.first_codepoint() {
                Some(ch) => ch.$from_fn(),
                None => false
            }
        }
    };

}

/// Takes a list form of the above nya
macro_rules! make_single_only_funcs {
    ($( $(#[$attrs:meta])* $name:ident = $from_fn:ident),*) => {
        $(make_mc_func!{$(#[$attrs])* single pub fn $name = $from_fn})*
    };
}

/// Takes a list form of the multichar forwards
macro_rules! make_multi_funcs {
    ($( $(#[$attrs:meta])* $name:ident = $from_fn:ident),*) => {
        $(make_mc_func!{$(#[$attrs])* multi pub fn $name = $from_fn})*
    };
}


impl GraphemeCluster {
    /// Unsafe because this does no checks for valid grapheme-ness nya
    pub(self) unsafe fn new(grapheme: &str) -> Self {
        Self (match grapheme.len() {
            0 => GraphemeClusterInternal::CodePoints0,
            1 => GraphemeClusterInternal::CodePoints1(maybe_take!(1, grapheme.chars()).unwrap()),
            2 => GraphemeClusterInternal::CodePoints2(maybe_take!(2, grapheme.chars()).unwrap()),
            3 => GraphemeClusterInternal::CodePoints3(maybe_take!(3, grapheme.chars()).unwrap()),
            4 => GraphemeClusterInternal::CodePoints4(maybe_take!(4, grapheme.chars()).unwrap()),
            5 => GraphemeClusterInternal::CodePoints5(maybe_take!(5, grapheme.chars()).unwrap()),
            6 => GraphemeClusterInternal::CodePoints6(maybe_take!(6, grapheme.chars()).unwrap()),
            _ => GraphemeClusterInternal::DynCodePoint(grapheme.to_string())
        })
    }

    /// Get the length of this function nya
    pub fn len(&self) -> usize {
        match &self.0 {
            GraphemeClusterInternal::CodePoints0 => {0}
            GraphemeClusterInternal::CodePoints1(ps) => {ps.len()}
            GraphemeClusterInternal::CodePoints2(ps) => {ps.len()}
            GraphemeClusterInternal::CodePoints3(ps) => {ps.len()}
            GraphemeClusterInternal::CodePoints4(ps) => {ps.len()}
            GraphemeClusterInternal::CodePoints5(ps) => {ps.len()}
            GraphemeClusterInternal::CodePoints6(ps) => {ps.len()}
            GraphemeClusterInternal::DynCodePoint(ps) => {ps.len()}
        }
    }

    /// Convert this grapheme into a plain string representation nya
    pub fn to_string(&self) -> String {
        match &self.0 {
            GraphemeClusterInternal::CodePoints0 => {String::new()}
            GraphemeClusterInternal::CodePoints1(ps) => {make_string_chars(ps)}
            GraphemeClusterInternal::CodePoints2(ps) => {make_string_chars(ps)}
            GraphemeClusterInternal::CodePoints3(ps) => {make_string_chars(ps)}
            GraphemeClusterInternal::CodePoints4(ps) => {make_string_chars(ps)}
            GraphemeClusterInternal::CodePoints5(ps) => {make_string_chars(ps)}
            GraphemeClusterInternal::CodePoints6(ps) => {make_string_chars(ps)}
            GraphemeClusterInternal::DynCodePoint(s) => {s.clone()}
        }
    } 

    pub fn chars(&self) -> GraphemeClusterIter<'_> {
        GraphemeClusterIter::new(self)
    }

    /// If this is a single-codepoint character, get that character, 
    /// else return none nya
    pub fn single_codepoint(&self) -> Option<char> {
        match self.len() == 1 {
            // no need to unwrap since we return an optional anyway nya
            true => self.chars().next(),
            false => None
        }
    }

    /// If this is a single codepoint and the given fn on that is
    /// true, then return true
    ///
    /// Else return false
    pub fn single_codepoint_matches(&self, criterion: impl FnOnce(char) -> bool) -> bool {
        self.single_codepoint().map_or(false, criterion)
    }

    /// Returns the first codepoint if this has a first codepoint nya
    pub fn first_codepoint(&self) -> Option<char> {
        match self.len() >= 1 {
            // no need to unwrap since we return optional
            true => self.chars().next(),
            false => None
        }
    }

    /// Holds true if the first codepoint is present and matches the criterion
    ///
    /// Else is false
    pub fn first_codepoint_matches(&self, criterion: impl FnOnce(char) -> bool) -> bool {
        self.first_codepoint().map_or(false, criterion)
    }
   
    make_single_only_funcs!{
        /// Check if this is an ascii grapheme (i.e. it has exactly one codepoint at the
        /// front that is ascii nyaa)
        is_single_ascii = is_ascii,
        is_single_ascii_alphabetic = is_ascii_alphabetic,
        is_single_ascii_uppercase = is_ascii_uppercase,
        is_single_ascii_lowercase = is_ascii_lowercase,
        is_single_ascii_alphanumeric = is_ascii_alphanumeric,
        is_single_ascii_digit = is_ascii_digit,
        is_single_ascii_hexdigit = is_ascii_hexdigit,
        is_single_ascii_punctuation = is_ascii_punctuation,
        is_single_ascii_graphic = is_ascii_graphic,
        /// Check if this is an ascii whitespace grapheme (i.e. it has exactly 1 
        /// codepoint at the front that is ascii whitespace)
        is_single_ascii_whitespace = is_ascii_whitespace,
        is_single_ascii_control = is_ascii_control,



        // unicode! nya
        is_single_alphabetic = is_alphabetic,
        is_single_uppercase = is_uppercase,
        is_single_lowercase = is_lowercase,
        is_single_alphanumeric = is_alphanumeric,
        is_single_numeric = is_numeric,
        /// Check if this is a unicode whitespace grapheme (i.e. it has 1 
        /// codepoint at the front that is unicode whitespace
        is_single_whitespace = is_whitespace,
        is_single_control = is_control
    }


    make_multi_funcs!{
        /// Check if the first character is ascii in this grapheme nya.
        /// Note that this may result in weirdness with multi-representation
        /// characters - which could look the same but have different representations
        /// in terms of combining marks and things like that nya
        is_first_ascii = is_ascii,
        is_first_ascii_alphabetic = is_ascii_alphabetic,
        is_first_ascii_uppercase = is_ascii_uppercase,
        is_first_ascii_lowercase = is_ascii_lowercase,
        is_first_ascii_alphanumeric = is_ascii_alphanumeric,
        is_first_ascii_digit = is_ascii_digit,
        is_first_ascii_hexdigit = is_ascii_hexdigit,
        is_first_ascii_punctuation = is_ascii_punctuation,
        is_first_ascii_graphic = is_ascii_graphic,
        /// Check if this is an ascii whitespace grapheme (i.e. it has 1 
        /// codepoint at the front that is ascii whitespace
        is_first_ascii_whitespace = is_ascii_whitespace,
        is_first_ascii_control = is_ascii_control,

        // unicode! nya
        is_first_alphabetic = is_alphabetic,
        is_first_uppercase = is_uppercase,
        is_first_lowercase = is_lowercase,
        is_first_alphanumeric = is_alphanumeric,
        is_first_numeric = is_numeric,
        /// Check if this is an ascii whitespace grapheme (i.e. it has 1 
        /// codepoint at the front that is ascii whitespace
        is_first_whitespace = is_whitespace,
        is_first_control = is_control
    }
}

impl std::fmt::Display for GraphemeCluster {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.to_string(), f) 
    }
}

impl std::hash::Hash for GraphemeCluster {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        for ch in self.chars() {
            state.write_u32(ch as u32);
        }
    }
}

impl PartialEq for GraphemeCluster {
    // currently no better way of doing this than with direct str
    // TODO: use some kinda unicode normalisation or something nya
    fn eq(&self, other: &Self) -> bool {
        iter_elem_eq(&mut self.chars(), &mut other.chars())
    }
}

impl Eq for GraphemeCluster{}

/// Holds a sequence of extended (always) graphemes nyaa.
/// Uses unicode_segmentation to generate them nyaaa,
/// as such all constraints on the output of the iterator
/// produced by `unicode_segment::UnicodeSegmentation::graphemes`
/// apply to the extended grapheme clusters in `GraphemeCluster`
#[derive(Debug)]
pub struct GraphemeClusterStr {
    /// ALLL THE EXTENDED GRAPHEME CLUSTERS ^.^ nya
    /// TODO: allow for slices of these things nya
    data: ConceptualSlice::<GraphemeCluster>
}

impl GraphemeClusterStr {
    pub fn new(our_str: &str) -> Self {
        Self {
            // true means extended graphemes
            // SAFETY:
            // * these graphemes are generated by a known-good library based on the 
            //   unicode database nya
            data: our_str.graphemes(true).map(|a| unsafe {GraphemeCluster::new(a)}).collect()
        }
    }

    /*
    /// This function takes an iterator over &str that MUST be created from a
    /// <some_str.graphemes(true).<some selector or transformer that doesn't change
    /// the contents of the values produced by the grapheme iterator nya>
    pub(self) unsafe fn from_grapheme_iter(grapheme_iter: &mut impl Iterator::<Item=&'bs str>) -> Self {
        Self {
            og_str: None,
            data: grapheme_iter.map(|a| GraphemeCluster::new(a)).collect()
        }
    }
    */
    
    /// Number of graphemes
    pub fn len(&self) -> usize {
        self.data.len()
    }
}

impl FromStr for GraphemeClusterStr {
    type Err = util::VoidErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self::new(s))
    }
}

impl <Idx: std::slice::SliceIndex::<[GraphemeCluster]>> std::ops::Index::<Idx> 
for GraphemeClusterStr {
    type Output = Idx::Output; 
    fn index(&self, index: Idx) -> &Self::Output {
        &self.data[index]
    }
}

impl std::fmt::Display for GraphemeClusterStr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for ch in self.data.as_slice().into_iter() {
            write!(f, "{}", ch)?;
        }
        Ok(())
    }
}

impl <'slicefrom> From::<&'slicefrom [GraphemeCluster]>
for GraphemeClusterStr {
    fn from(d: &'slicefrom [GraphemeCluster]) -> Self {
        Self {
            data: ConceptualSlice::Vec(d.into_iter().map(|a| a.clone()).collect()),           
        }
    }
}

impl <'a> IntoIterator for &'a GraphemeClusterStr {
    type Item = &'a GraphemeCluster;

    type IntoIter = <&'a Vec::<GraphemeCluster> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.data.into_iter()
    }
}

impl Parse for GraphemeClusterStr {
    type PositionRepr = usize;

    fn start<'input>(&'input self) -> usize {
        0
    }

    fn position_repr<'input>(&'input self, p: usize) -> Self::PositionRepr {
        p
    }
}

impl ParseElem for GraphemeClusterStr {
    type Element = GraphemeCluster;

    fn parse_elem(&self, pos: usize) -> peg::RuleResult<Self::Element> {
        // Taken from https://docs.rs/peg-runtime/0.6.3/src/peg_runtime/slice.rs.html#3-12
        match self.data.get(pos) {
            Some(clust) => peg::RuleResult::Matched(pos + 1, clust.clone()),
            None => peg::RuleResult::Failed
        }
    }
}

impl ParseLiteral for GraphemeClusterStr {
    fn parse_string_literal(&self, pos: usize, literal: &str) -> peg::RuleResult<()> {
        // Length checks taken from https://docs.rs/peg-runtime/0.6.3/src/peg_runtime/slice.rs.html#3-12
        let raw_graphemes = literal.graphemes(true);
        // this takes exactly one more than the number of elements in the current string after the
        // given position nya
        // 
        // SAFETY:
        //  * graphemes generated from known-good library :) nya
        let mut graphemes_up_to_end = raw_graphemes.map(
            |a| unsafe {GraphemeCluster::new(a)}
        ).take(self.data[pos..].len() + 1);
        let mut rest_of_our_graphemes = self.data[pos..].iter();
        // number of iterations. On successful match we add this to the position since it is the
        // number of consumed graphemes nya
        let mut shift = 0usize;
        loop {
            let cluster_pair = (rest_of_our_graphemes.next(), graphemes_up_to_end.next());
            match cluster_pair {
                // If we just have two clusters we need to match them nya
                (Some(our_cluster), Some(matched_cluster)) => {
                    // match fail
                    if *our_cluster != matched_cluster {
                        return peg::RuleResult::Failed;
                    } else {
                        // consume a character
                        shift += 1;
                    }
                },
                // No more characters to check.
                // If we got to this point we matched all characters up to this point
                // In the null matcher case this does not shift the position and as such it should
                // not in any other case either nya. 
                (Some(_our_cluster), None) => {
                    return peg::RuleResult::Matched(pos + shift, ());
                },
                // ran out of graphemes before the end of the literal, there cannot be a match nya
                (None, Some(_checked_cluster)) => {
                    return peg::RuleResult::Failed;
                },
                // if we started right at the end/one-after i.e. the position was self.len(), and grabbed
                // None from the iterator, and there was an empty matching literal, the shift would
                // be zero and we would need the position to remain the same.
                //
                // If we consumed exactly one character at the end the shift would be 1 and 
                // output position would be pos + (shift = 1) (i.e. one after the end nyaa),
                // so we do NOT want to increment shift nya)
                (None, None) => {
                    return peg::RuleResult::Matched(pos + shift, ());
                }
            }
        }
    }
}

impl <'bs> ParseSlice<'bs> for GraphemeClusterStr {
    type Slice = GraphemeClusterStr;

    fn parse_slice(&'bs self, p1: usize, p2: usize) -> Self::Slice {
       self.data.as_slice()[p1..p2].into() 
    }
}


/// Holds some useful unicode categories nya
///
/// Gets the major categories from minor categories as per
/// https://en.wikipedia.org/wiki/Unicode_character_property#General_Category
#[allow(dead_code)]
pub mod ucd_major_cats {
    macro_rules! true_for_categories {
        ($match:ident, $($categories:ident),*) => {
            match $match.category() {
                $(::ucd::UnicodeCategory::$categories => {true})*
                _ => false
            }
        }
    }

    macro_rules! category_functions {
        ($( $(#[$attrs:meta])* $name:ident = $($categories:ident)|* );*) => {
            $(
                $(#[$attrs])*
                pub fn $name(codepoint: impl ::ucd::Codepoint) -> bool {
                    true_for_categories!{
                        codepoint,
                        $($categories),*
                    }
                }
                
            )*
        }
    }

    category_functions!{ 
        /// Check if a codepoint is in the major unicode category of 
        /// letter
        is_letter = 
            UppercaseLetter|
            LowercaseLetter|
            TitlecaseLetter|
            ModifierLetter|
            OtherLetter;

        /// Check if a codepoint is in the major unicode category of
        /// Marks nya
        is_mark = 
            NonspacingMark|
            SpacingMark|
            EnclosingMark;

        /// Check if a codepoint is in the major unicode category of
        /// Number
        is_number =
            DecimalNumber|
            LetterNumber|
            OtherNumber;

        /// Check if a codepoint is in the major unicode category of
        /// Punctuation
        is_punctuation = 
            ConnectorPunctuation|
            DashPunctuation|
            OpenPunctuation|
            ClosePunctuation|
            InitialPunctuation|
            FinalPunctuation|
            OtherPunctuation;

        /// Check if a codepoint is in the major unicode category of
        /// Symbol
        is_symbol =
            MathSymbol|
            CurrencySymbol|
            ModifierSymbol|
            OtherSymbol;

        /// Check if a codepoint is in the major unicode category of
        /// Separator
        is_separator = 
            SpaceSeparator|
            LineSeparator|
            ParagraphSeparator;

        /// Check if a codepoint is in the major unicode category of
        /// Other
        is_other = 
            Control|
            Format|
            Surrogate|
            PrivateUse|
            Unassigned
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum SizeParseErr {
    /// non ascii 0-9 char found nya
    InvalidGrapheme(GraphemeCluster),
    /// Overflow occured when trying to parse ;3
    NumberTooLarge
}

pub fn parse_10s_digits_usize_pos(a: &GraphemeClusterStr) 
    -> Result::<usize, SizeParseErr> {
    let mut res = 0usize;
    for cluster in a {
        let codepoint = cluster.single_codepoint().ok_or(
           SizeParseErr::InvalidGrapheme(cluster.clone())
        )?;
        let decimal_digit =util::parse_decimal_digit(codepoint).ok_or(
            SizeParseErr::InvalidGrapheme(cluster.clone())
        )?;
        res = res.checked_mul(10).ok_or(SizeParseErr::NumberTooLarge)?;
        res = res.checked_add(decimal_digit as usize).ok_or(SizeParseErr::NumberTooLarge)?;
   }
   Ok(res)
}


/*
fractaliser4 - fractal generator
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
