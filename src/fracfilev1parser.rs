use peg;
use crate::grapheme_peg::*;
use crate::grapheme_peg;
use crate::grapheme_peg::ucd_major_cats as ucats;
use crate::util;
use crate::ndarray::prelude::*;
use crate::ndarray;
use std::collections::hash_map::HashMap;
use std::collections::hash_set::HashSet;
use patsubgen::internal_state::ValidatedPatternSubstitution;
use translator;

// #[derive(Parser)]
// #[grammar = "pest/frac4fileV1.pest"]
// pub struct FracFileV1Parser;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct PatSize2D {
    pub xsize: std::num::NonZeroUsize,
    pub ysize: std::num::NonZeroUsize
}

impl PatSize2D {
    pub fn to_ndarraysize(&self) -> Ix2 {
        Ix2(self.xsize.get(), self.ysize.get())
    }
}

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct State(GraphemeCluster);

/// Holds a pattern specification nya
#[derive(Clone, Debug)]
pub struct Pat2DMap {
    pub data: HashMap::<State, ndarray::Array2::<State>>
}

impl Default for Pat2DMap {
    fn default() -> Self {
        Pat2DMap {data: Default::default()}
    }
}

/// Holds data for the basic substitution nya
#[derive(Clone, Debug)]
pub struct BasicSubstitution {
    initial_pattern_size: PatSize2D,
    initial_pattern: ndarray::Array2::<State>,
    pattern_size: PatSize2D,
    substitutions: Pat2DMap,
    allowed_states: HashSet::<State>,
    substitution_count: usize
}

impl ValidatedSubs {
    /// override some colours in the palette
    ///
    /// Errors if the state in question is an invalid state for this palette
    pub fn override_palette(&mut self, state: &State, colour: util::Colour) -> Result::<(), ()> {
        self.0.1.get_mut(state).map(|a| *a = colour).ok_or(())
    }

    /// Override the substitution count nya
    pub fn override_substitution_count(&mut self, subcount: usize) {
        self.0.0.substitution_count = subcount;
    }
}

/// Holds a validated substitution set with a palette nya
#[derive(Clone, Debug)]
pub struct ValidatedSubs((BasicSubstitution, HashMap::<State, util::Colour>)); 


peg::parser!{pub grammar grapheme_pattern_parser() for GraphemeClusterStr {
    /// whitespace characters nya
    rule _() = [n if n.is_first_whitespace()]
    
    /// Matches any grapheme cluster nya
    rule any() = [_]
    
    /// Detect comments
    rule comment() = "/" (!"/" any())* "/"

    /// Any thing which should be ignored nyaaaaa
    //rule __() = (comment() / _)*
    rule __() = (_/comment()/_)*

    /// Parse a single hex digit and return nya
    rule hex_digit() -> util::Hex = a:$([n if n.is_single_ascii_hexdigit()]) {
        // parsing rules ensure safety nya
        // We know there is only one grapheme with the given test run on 
        // it so we can just index ^.^ nya
        util::parse_hex_digit(a[0].first_codepoint().unwrap()).unwrap()
    }

    /// Parse a byte written as two hex characters adjacent nya
    rule hex_byte() -> u8 = msbs:hex_digit() lsbs:hex_digit() {
        (msbs as u8) << 4 | (lsbs as u8) 
    }

    rule rgb() -> util::RGB = r:hex_byte() g:hex_byte() b:hex_byte() {
        util::RGB {r, g, b}
    }

    /// Parse rgba ^.^ nyaa
    rule rgba() -> util::RGBA = r:hex_byte() g:hex_byte() b:hex_byte() a:hex_byte() {
        util::RGBA {r, g, b, a}
    }

    rule col_rgba() -> util::Colour = "#" col:rgba() {
        util::Colour::RGBA(col)
    }

    rule col_rgb() -> util::Colour = "#" col:rgb() {
        util::Colour::RGB(col)
    }
    
    /// parse a colour out of either #RRGGBB or #RRGGBBAA
    rule colour() -> util::Colour = (col_rgba() / col_rgb())

    /// take a single character holding a state nya
    rule state_character() -> State = d:$(
            !(_/";"/"="/">"/"<"/"#"/":"/"/") [
                n if n.first_codepoint_matches(|c|
                    ucats::is_symbol(c) | 
                    ucats::is_letter(c) | 
                    ucats::is_number(c) | 
                    ucats::is_punctuation(c)
                )
            ]
        ) {
        State(d[0].clone())
    }

    /// Take a single state in the allowed states nya
    /// When states are built it inherently excludes invalid characters nya
    rule valid_state(allowed_states: &HashSet<State>) -> State
        = r:$([n if allowed_states.contains(&State(n.clone()))])  {
            //println!("valid states: {:?}", allowed_states);
            State(r[0].clone())
        }

    /// Pull a nonnegative int size nya
    /// and convert into usize ;3
    rule size() -> usize = d:$([n if n.is_single_ascii_digit()]+) {?
        let res = grapheme_peg::parse_10s_digits_usize_pos(&d);
        match res {
            Ok(size) => Ok(size),
            Err(SizeParseErr::InvalidGrapheme(g)) => unreachable!("filter above should stop this nya"),
            Err(SizeParseErr::NumberTooLarge) => {
                Err("Size provided too large nya")
            }
        }
    }

    /// Get a positive, nonzero size.
    rule nonzero_size() -> std::num::NonZeroUsize = !"0" num:size() {
        std::num::NonZeroUsize::new(num).unwrap()
    }

    /// X size specifier
    rule x_size_spec() -> std::num::NonZeroUsize = 
        "xs" __ ":" __ x:nonzero_size() __ ";" {x}
    /// Y size specifier nya
    rule y_size_spec() -> std::num::NonZeroUsize = 
        "ys" __ ":" __ y:nonzero_size() __ ";" {y}
    
    /// Matches specifiers in xy order
    rule xy_size_spec() -> PatSize2D 
        = __ xsize:x_size_spec() __ ysize:y_size_spec() __ {
        PatSize2D {xsize, ysize}
    }

    /// Matches specifiers in yx order nya
    rule yx_size_spec() -> PatSize2D 
        = __ ysize:y_size_spec() __ xsize:x_size_spec() __ {
        PatSize2D {xsize, ysize}
    }
    
    /// Matches size specifiers.
    rule size_spec() -> PatSize2D = xy_size_spec() / yx_size_spec()

    /// Matches the list of available states nya
    rule available_states() -> HashSet<State> = 
        __ "states" __ ":" __  r:(state_character()++(__)) ";" __ {
        //println!("got states: {:?}", r);
        let res: HashSet::<State> = r.into_iter().collect();
        res
    }

    /// get a pattern of size nya.
    rule pattern(size: PatSize2D, allowed_states: &HashSet<State>) -> ndarray::Array2<State>
        = rows: ( __
            row: (
                __ r:valid_state(allowed_states) {r} 
            )*<{size.xsize.get()}> __ ";" {row}
        )*<{size.ysize.get()}> {
        ndarray::Array2::from_shape_fn(
            (size.xsize.get(), size.ysize.get()), 
            |(column, row)| {
                rows[row][column].clone()
            }
        )
    }
    
    /// Get a pair of (State, Pattern) nya
    rule pattern_substitution(size: PatSize2D, allowed_states: &HashSet<State>) 
        -> (State, ndarray::Array2<State>) = 
        instate:valid_state(allowed_states) __ "=>" 
            __ pat:pattern(size, allowed_states) __ {
        (instate, pat)
    }
   
    /// Get the substitutions nya
    rule pattern_subs(sub_size: PatSize2D, allowed_states: &HashSet<State>) -> Pat2DMap 
        = the_subs: (the_sub:pattern_substitution(sub_size, allowed_states) __ {the_sub})+ __ {
        Pat2DMap {
            data: the_subs.into_iter().collect()
        }
    }


    /// Specifies the basic substitution data nya
    pub rule substitution_specifier() -> BasicSubstitution
        = 
        allowed_states:available_states() __ 

        "pattern_size" __ ":" __
        pattern_size:size_spec() __

        "substitutions" __ ":" __ 
        substitutions:pattern_subs(pattern_size, (&allowed_states)) __

        "initial" __ ":" __
        initial_pattern_size: size_spec() __
        initial_pattern: pattern(initial_pattern_size, (&allowed_states)) __ 

        "subs" __ ":" __
        substitution_count:size() __ ";" {
            BasicSubstitution {
                allowed_states,
                
                pattern_size,
                substitutions,

                initial_pattern_size,
                initial_pattern,
                substitution_count
            }
        }

    /// Grabs a (state, colour) pair nya
    rule single_colour_map(allowed_states: &HashSet<State>) -> (State, util::Colour)
        = __ state: valid_state(allowed_states) __ ":" __ col:colour() __ ";" __ { 
            (state, col)
        }

    rule palette_list(allowed_states: &HashSet<State>) -> HashMap<State, util::Colour> = 
        __ pairs:single_colour_map(allowed_states)++__ __{
        pairs.into_iter().collect() 
    }

    /// The actual parser! nya
    ///
    /// This does some extra checks for self consistency, in particular
    /// it ensures that the palette contains colours for every state that
    /// may be used and that all available states have a substitution nya.
    ///
    /// Other restrictions were already checked by the grammar ;p nya
    pub rule ffv1() -> ValidatedSubs
        = __ "v1" __ // magic specifier 
        subs:substitution_specifier() __
        "default_palette" __ ":" __
        pal:palette_list((&subs.allowed_states)) __
        ![_] {?
            if !util::has_keys_for_all_in(&subs.substitutions.data, &subs.allowed_states) {
                Err("No substitution for some states specified! nya")
            } else if !util::has_keys_for_all_in(&subs.substitutions.data, &subs.allowed_states) {
                Err("Missing default colour for state")
            } else {
                Ok(ValidatedSubs((subs, pal)))
            }
        }
}}

use patsubgen::internal_state::{
    PatternSize,
    Pattern,
    InternalState
};

/// Linearised-mapping version of the substitutions :) nya
///
/// This essentially is the result of translating a pattern into the more efficient form for
/// pattern substitution processing nya
pub struct LinearisedSubstitution {
    initial_pattern_size: PatternSize::<Ix2>,
    initial_pattern: ndarray::Array2::<InternalState>,
    substitution_pattern_size: PatternSize::<Ix2>,
    linearised_state_translator: translator::TranslationTable::<State>,
    linearised_substitutions: Vec::<Pattern::<Ix2>>,
    pure_subs: patsubgen::substitution::Substitutions::<Ix2>,
    linearised_palette: Vec::<util::Colour>,
    subcount: usize
}


pub fn to_pat_size(size: PatSize2D) -> patsubgen::internal_state::PatternSize::<Ix2> {
    PatternSize::<Ix2>::from_size(Ix2(size.xsize.get(), size.ysize.get()))
}

/// Transform the validated substitutions into a linearised form for efficient 
/// indexing. This implicitly generates a linearised state mapping (abstractly, not
/// the LinearisedMap object nya) for input states and does some transformations
pub fn linearise_subs(grammar_out: ValidatedSubs) -> LinearisedSubstitution {
    let (basic_sub, palette) = (grammar_out.0 .0, grammar_out.0 .1);
    let subcount = basic_sub.substitution_count;
    let linearised_state_translator = translator::TranslationTable::new(basic_sub.allowed_states);
    // unparsed substitutions, so not in Pattern form but raw ndarray form nya
    let (linearised_substitutions_raw, excess) = linearised_state_translator
        .raw_translate(basic_sub.substitutions.data)
        .expect("Grammar checks that every state has a substitution, 
                this should be unreachable nya");
    if excess.len() > 0 {
            panic!("Excess linear substitutions without valid instate are removed by grammar");
    }

    let glob_trans = |s: &State| 
        linearised_state_translator
        .get_linear_state(s)
        .expect("Grammar ensures that no invalid states are in subs");

    let linearised_substitutions: Vec::<_> = linearised_substitutions_raw.into_iter()
        .map(|raw_pattern| 
            patsubgen::internal_state::Pattern::<Ix2>::new(
                raw_pattern.map(
                    |s| InternalState::new(&glob_trans(s))
                )
            )
        )
        .collect();

    let (linearised_palette, excess) = linearised_state_translator
        .raw_translate(palette)
        .expect("Grammar checks that every state has a default palette colour nya");

    if excess.len() > 0 {
        panic!("Palette for invalid state! This should be checked by the grammar nya");
    }

    let initial_pattern = basic_sub.initial_pattern.map(|s| 
        InternalState::new(&glob_trans(s))
    );

    let initial_pattern_size = to_pat_size(basic_sub.initial_pattern_size);

    let substitution_pattern_size = to_pat_size(basic_sub.pattern_size);

    let validated_subs = ValidatedPatternSubstitution::new_or_err((
        linearised_substitutions.clone(), 
        substitution_pattern_size.clone()
    )).expect("We already did the work for self-consistency validation in the grammar nya");
    let pure_subs = patsubgen::substitution::Substitutions::new(&validated_subs);

    LinearisedSubstitution {
        initial_pattern_size,
        initial_pattern,
        substitution_pattern_size,
        linearised_state_translator,
        linearised_substitutions,
        linearised_palette,
        pure_subs,
        subcount 
    }
}

impl LinearisedSubstitution {
    /// Create a pattern generator out of this struct that produces
    /// the output in the given order nya
    pub fn make_pattern_generator<
        MemOrder: patsubgen::substitution::gen::GenMemoryOrder
    > (
        &self, output_order: MemOrder
    ) -> patsubgen::substitution::gen::PatternGenerator::<'_, '_, Ix2, Ix2, MemOrder> {
        patsubgen::substitution::gen::PatternGenerator::new_or_err(
            self.initial_pattern.view(),
            &self.pure_subs,
            self.subcount,
            output_order
        ).expect("Everything is 2D, no need to worry about ndim errors nya")
    }
    
    /// Get the linearised map of the palette
    ///
    /// Translation table is retreivable via [`state_translation_table`] nya
    pub fn palette_linearised_map(&self) -> &[util::Colour] {
        &self.linearised_palette
    }

    /// Get the state translation table for user provided states <-> indices (InternalState)
    /// nyaa
    pub fn state_translation_table(&self) -> &translator::TranslationTable::<State> {
        &self.linearised_state_translator
    }

    /// Get the linearised map for the substitutions (with the internals converted 
    /// to linearised-map-based [`InternalState`]s nya).
    pub fn substitution_linearised_map(&self) -> &Vec::<patsubgen::internal_state::Pattern::<Ix2>> {
        &self.linearised_substitutions
    }

    /// Change the substitution count of the next PatSubGen fed out of this thing nya
    pub fn set_substitution_count(&mut self, subcount: usize) {
        self.subcount = subcount;
    }


}


/*
fractaliser4 - fractal generator
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
