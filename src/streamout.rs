#![allow(dead_code)]
use patsubgen::internal_state::InternalState;
use crate::util::{
    Colour, 
    RGB, 
    RGBA, 
    RGBAIter, 
    RGBIter, 
    ChunkIterExtension, 
    ChunkReducer
};
use crate::util;


/// Bits-per-pixel options for palette-based PNG images nya
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum PaletteBitDepth {
    One = 1,
    Two = 2,
    Four = 4,
    Eight = 8
}

impl Into::<png::BitDepth> for PaletteBitDepth {
    fn into(self) -> png::BitDepth {
        match self {
            PaletteBitDepth::One => png::BitDepth::One,
            PaletteBitDepth::Two => png::BitDepth::Two,
            PaletteBitDepth::Four => png::BitDepth::Four,
            PaletteBitDepth::Eight => png::BitDepth::Eight
        }
    }
}

impl PaletteBitDepth {
    /// Max # of colours in a palette of a given size nya
    pub const fn max_palette_size(&self) -> u16 {
        match self {
            PaletteBitDepth::One => 2,
            PaletteBitDepth::Two => 4,
            PaletteBitDepth::Four => 16,
            PaletteBitDepth::Eight => 256
        }
    }

    /// Get the number of bytes required for storing pixels of this
    /// bit depth nya
    pub const fn bytes_for(&self, pixels: usize) -> usize {
        let (bytes, remaining_bits) = (
            pixels / pixels_per_byte(*self) as usize, 
            pixels % pixels_per_byte(*self) as usize
        );
        match remaining_bits {
            0 => bytes,
            _ => bytes + 1 
        }
    }
}

/// Holds info about PNG palette configuration and transparency nya ^.^
#[derive(Clone, Eq, PartialEq)]
pub enum PaletteConfigIntrnl {
    OpaquePalette {
        // The bit depth of the palette
        depth: PaletteBitDepth,
        /// The rgb palette sections nya, bytes in RGB order nyaaaa
        rgb_palette_section: Vec::<u8>
    },
    TransparentPalette {
        /// Bit depth  of the palette
        depth: PaletteBitDepth,
        /// RGB palette - bytes are in RGB order nya
        rgb_palette_section: Vec::<u8>,
        /// The transparency section (TRNS section in PNG, which acts like palette in palette mode
        /// nya)
        opacity_palette_section: Vec::<u8>
    },
    OpaqueColour {
        /// "Our palette" of colours for fractal states nya, though of course this happens
        /// when you cannot use an in-format PNG palette nya.
        state_colours: Vec::<RGB>
    },
    TransColour {
        /// Like above but with actual transparent colours ;3
        state_colours: Vec::<RGBA>
    }
}


/// Holds an internal palette configuration nya
#[derive(Clone, Eq, PartialEq)]
pub struct PaletteConfig {
    config: PaletteConfigIntrnl
}

impl PaletteConfig {
    /// Take a linearised-map palette nya
    /// (linearised_map as produced generally by the translator library
    /// or more generally a list of colours where the index is the state
    /// in some other abstract software palette nya)
    pub fn new(linearised_map_palette: &[Colour]) -> Self {
        let needs_transparency = linearised_map_palette
            .into_iter()
            .any(|c| match c {
                Colour::RGB(_) => false,
                Colour::RGBA(RGBA {r: _, g: _, b: _, a}) if *a != 255 => true,
                Colour::RGBA(_) => false
            });

        // if this is some then we can use palettes
        // else we have to use actual plain colours nya
        let index_bit_depth = match linearised_map_palette.len() {
            n if n <= 2 => Some(PaletteBitDepth::One),
            n if n <= 4 => Some(PaletteBitDepth::Two),
            n if n <= 16 => Some(PaletteBitDepth::Four),
            n if n <= 256 => Some(PaletteBitDepth::Eight),
            _ => None
        };
        
        let config = match index_bit_depth {
            Some(bitdepth) => {
                // Pull out rgb and discard transparency nya
                let non_transparent_palette: Vec::<_> = linearised_map_palette
                    .into_iter()
                    .map(|a| a.discarded_transparency())
                    .flatten()
                    .collect();
                match needs_transparency {
                    true => {
                        let transparency_palette: Vec::<_> = linearised_map_palette
                            .into_iter()
                            .map(|col| col.with_transparency())
                            .map(|rgba| rgba.a)
                            .collect();
                        PaletteConfigIntrnl::TransparentPalette {
                            depth: bitdepth,
                            opacity_palette_section: transparency_palette,
                            rgb_palette_section: non_transparent_palette
                        }
                    },
                    false => PaletteConfigIntrnl::OpaquePalette {
                        depth: bitdepth,
                        rgb_palette_section: non_transparent_palette
                    }
                }
            }
            None => match needs_transparency {
                true => PaletteConfigIntrnl::TransColour {
                    state_colours: linearised_map_palette
                        .into_iter()
                        .map(|a| a.with_transparency())
                        .collect()
                },
                false => PaletteConfigIntrnl::OpaqueColour {
                    state_colours: linearised_map_palette
                        .into_iter()
                        .map(|a| a.discarded_transparency())
                        .collect()
                }
            }
        };
        Self {config}
    }


    /// Get an iterator that produces png-encoded bits
    /// Note that bytes cannot be packed across rows, and if a row size is provided
    /// then a count will be kept so rows are split nya,
    /// For RGB/RGBA (non paletted) this is technically not necessary but still nya
    ///
    /// Rowsize must either be None, or nonzero int nya (or expect a panic)
    pub fn make_byte_iterator_for<'a>(
        &'a self,
        linstate_iter: impl Iterator::<Item=InternalState> + 'a,
        rowsize: Option::<u32>
    ) -> impl 'a + Iterator::<Item=u8> {
        match &self.config {
            PaletteConfigIntrnl::OpaquePalette { 
                depth, 
                rgb_palette_section: _ 
            } => util::TriIterator::Left(
                png_byte_packer(linstate_iter, *depth, rowsize)
            ),
            PaletteConfigIntrnl::TransparentPalette { 
                depth, 
                rgb_palette_section: _, 
                opacity_palette_section: _ 
            } => util::TriIterator::Left(
                png_byte_packer(linstate_iter, *depth, rowsize)
            ),
            PaletteConfigIntrnl::OpaqueColour { state_colours } => util::TriIterator::Middle(
                linstate_iter.map(move |s| state_colours[s.0]).flatten()
            ),
            PaletteConfigIntrnl::TransColour { state_colours } => util::TriIterator::Right(
                linstate_iter.map(move |s| state_colours[s.0]).flatten()
            )
        }
    }
    
    /// Configure a `png::Encoder` appropriately for this configuration nya
    /// This configures:
    /// * Width and height nya
    /// * set_palette
    /// * set_trns
    /// * set_color
    /// * set_depth
    /// This does not modify (and as such modifying will not bork up the other
    /// processing functions based on this nya):
    /// * set_compression
    /// * set_filter
    pub fn configure<W: std::io::Write>(&self, w: W, sizex: u32, sizey: u32) 
        -> png::Encoder::<W>{
        let mut encoder = png::Encoder::new(w, sizex, sizey);
        match &self.config {
            PaletteConfigIntrnl::OpaquePalette { 
                depth, 
                rgb_palette_section 
            } => {
                encoder.set_palette(rgb_palette_section.clone());
                encoder.set_color(png::ColorType::Indexed);
                encoder.set_depth((*depth).into());
            }
            PaletteConfigIntrnl::TransparentPalette { 
                depth, 
                rgb_palette_section, 
                opacity_palette_section 
            } => {
                encoder.set_palette(rgb_palette_section.clone());
                encoder.set_trns(opacity_palette_section.clone());
                encoder.set_depth((*depth).into());
                encoder.set_color(png::ColorType::Indexed);
            }
            PaletteConfigIntrnl::OpaqueColour { state_colours: _ } => {
                encoder.set_color(png::ColorType::RGB);
                encoder.set_depth(png::BitDepth::Eight); // RGB with 1byte per pixel
                // nya
            }
            PaletteConfigIntrnl::TransColour { state_colours: _ } => {
                encoder.set_color(png::ColorType::RGBA);
                encoder.set_depth(png::BitDepth::Eight);
            }
        }
        encoder
    }

    /// Get the bytes for a single PNG row with this config
    ///
    /// Due to the way the png format works, data cannot 
    /// be trans-row, and as such this can be turned into 
    /// total byte count trivially (since no trans-row packing
    /// can occur nya)
    pub fn row_bytes(&self, sizex: u32) -> usize {
        match self.config {
            PaletteConfigIntrnl::OpaquePalette { depth, .. } => 
                depth.bytes_for(sizex as usize),
            PaletteConfigIntrnl::TransparentPalette { depth, .. } => 
                depth.bytes_for(sizex as usize),
            // RGB @ 1 byte/channel
            PaletteConfigIntrnl::OpaqueColour { state_colours: _ } => (sizex * 3) as usize,
            // RGBA @ 1 byte/channel nya
            PaletteConfigIntrnl::TransColour { state_colours: _ } => (sizex * 4) as usize
        }
    }

    /// Get the maximum raw image data bytes used for an image of the given
    /// size with this configuration nya
    ///
    /// Note that in PNG, you cannot pack trans-row data in a byte, 
    /// nyaa, so we have to do a little funkiness here to give the right value nya
    pub fn max_bytes(&self, sizex: u32, sizey: u32) -> usize {
        self.row_bytes(sizex) * sizey as usize
    }

}

/// Represents the # of pixels per byte of stream output nya
/// Obviously does not work if we have truecolour/non-png-palette 
/// data nya
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug)]
#[repr(u8)]
pub enum PixelsPerByte {
    Eight = 8,
    Four = 4,
    Two = 2,
    One = 1
}

impl Into::<std::num::NonZeroUsize> for PixelsPerByte {
    fn into(self) -> std::num::NonZeroUsize {
        // Enum does not have zero elements 
        std::num::NonZeroUsize::new(self as usize).unwrap()
    }
}

/// Get the pixels per byte for the given bit depth nya
pub const fn pixels_per_byte(bit_depth: PaletteBitDepth) -> PixelsPerByte {
    match bit_depth {
        // One bit per pixel means 8 pixels per byte nya
        PaletteBitDepth::One => PixelsPerByte::Eight,
        PaletteBitDepth::Two => PixelsPerByte::Four,
        PaletteBitDepth::Four => PixelsPerByte::Two,
        PaletteBitDepth::Eight => PixelsPerByte::One
    }
}


/// Little enum for holding the state of a currently-being-made byte nya
#[derive(Copy, Clone, Debug)]
enum PackerState {
    NoStoredPx(PaletteBitDepth), // like having an empty byte and byte_shift of 8 (put in first byte slot nya
    StoredPx {
        /// The shift used for the last pixel inputted before current nya
        last_in_byte_shift: u8, 
        /// The bits per pixel nya
        bpp: PaletteBitDepth, 
        px_byte: u8
    }
}

enum Finalisable {
    Data(InternalState),
    // no more data after this, spit out what you have left if 
    // any nya. This also acts as a reset button nya
    DoFinalise
}

impl PackerState {
    /// Feed more data into the packer state and occasionally
    /// emit a new byte nya
    fn feed(&mut self, data: Finalisable) -> Option::<u8> {
        match (&self, data) {
            (PackerState::NoStoredPx(bpp), Finalisable::Data(to_add)) => {
                let first_shift = 8u8 - (*bpp as u8);
                *self = PackerState::StoredPx {
                     last_in_byte_shift: first_shift ,
                     bpp: *bpp,
                     px_byte: (to_add.0 as u8) << first_shift
                };
                None
            },
            (PackerState::StoredPx {
                last_in_byte_shift, 
                bpp, 
                px_byte
            }, Finalisable::Data(to_add)) => {

                let new_shift = *last_in_byte_shift - (*bpp as u8); // if this is exactly 
                // filling the last <bits-per-pixel> byte chunks then we want to spit out a new
                // byte nya
                let new_byte = *px_byte | (to_add.0 as u8) << new_shift;
                if new_shift == 0 { // We are filling the last bit nya
                    // completed this byte nya
                    *self = PackerState::NoStoredPx(*bpp);
                    Some(new_byte)
                } else {
                    *self = PackerState::StoredPx {
                        last_in_byte_shift: new_shift,
                        px_byte: new_byte,
                        bpp: *bpp
                    };
                    None
                }
            },
            (PackerState::NoStoredPx (..), Finalisable::DoFinalise) => {
                None
            }
            (PackerState::StoredPx {
                last_in_byte_shift: _, 
                bpp, 
                px_byte
            }, Finalisable::DoFinalise) => {
                let res = *px_byte;
                *self = PackerState::NoStoredPx(*bpp);
                Some(res)
            }
        }
    }
}

/// Little implementation function for fusing bytes together from a linearstate 
/// iterator nya
///
/// https://docs.rs/png/0.16.7/src/png/encoder.rs.html#549-555
/// for streaming out using less-than-byte shifts nya
///
/// Note that if an optional row length is provided then this will work
/// to ensure byte packing does not occur between pixel rows nyaa
///
/// If the row length is zero it will panic! since that isn't allowed nya
fn png_byte_packer(
    in_iter: impl Iterator::<Item=InternalState>, 
    bits_per_pixel: PaletteBitDepth,
    rowlength: Option::<u32>
) -> impl Iterator::<Item=u8> {


    fn raw_packer_gen(it: impl Iterator::<Item=Finalisable>, bits_per_pixel: PaletteBitDepth) 
        -> impl Iterator::<Item=u8> { 
        let mut curr_bytepacker_state = PackerState::NoStoredPx(
            bits_per_pixel
        );
        it.filter_map(move |s| {
            curr_bytepacker_state.feed(s)
        }) 
    }

    match rowlength {
        Some(size) => {
            if size == 0 { panic!("zero-sized rows not allowed. Nya") };
            let finalising_data = 
                // SAFETY: we have just checked for zero-size chunks above ;3 nya
                unsafe {in_iter.as_raw_chunked(size as usize) }.map(|a| {
                    // convert this chunks into, essentially, finalising blocks nya
                    match a {
                        util::ChunkingIterComponent::Data(d) => Finalisable::Data(d),
                        util::ChunkingIterComponent::BlockMarker => Finalisable::DoFinalise
                    }
                });

            util::BiIterator::Left(raw_packer_gen(finalising_data, bits_per_pixel))
        },
        None => util::BiIterator::Right(raw_packer_gen(
            in_iter
            .map(|a| Finalisable::Data(a))
            .chain(Some(Finalisable::DoFinalise)),
             bits_per_pixel
        ))
    }
}

pub trait BytePackerExt : Iterator::<Item=InternalState> + Sized {
    // TODO: when rust allows 
    //
    //fn png_bytepack(self, bits_per_pixel: PaletteBitDepth) -> impl Iterator::<Item=u8> {
    //    png_byte_packer(self, bits_per_pixel)
    //} 
}

impl <T: Iterator::<Item=InternalState> + Sized> BytePackerExt for T {}







/*
fractaliser4 - fractal generator
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

