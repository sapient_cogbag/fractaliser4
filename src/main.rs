mod fractaldata;
mod fracfilev1parser;
#[macro_use]
mod util;
mod grapheme_peg;
mod streamout;

use patsubgen::ndarray;
use patsubgen::substitution::gen;
use grapheme_peg::GraphemeClusterStr;

use structopt::*;

use util::ChunkIterExtension;

static GRAMMAR_EXAMPLE: &str = r#"
v1 /<This is a magic version value and this is a comment/
states: 012; /the states for fractal substitution nya/

/the size of the patterns in the x and y direction/
pattern_size:
xs:2;
ys:2;

/Patterns to substitute/
/Note that every valid state must have a substitution/
/even if it cannot appear in the outputs of other states nya/
substitutions:
0 =>
01;
12;

1 =>
11;
11;

2 =>
22;
21;

/This holds the initial pattern size and values for the substitutions/
initial:
xs:2;
ys:2;

00;
01;
/the number of substitutions to perform nya/
subs: 8;


/These are the colours, in #RRGGBB or #RRGGBBAA format, that are used/
/by default for each state nya/
default_palette:
0: #000000;
1: #ffffff;
2: #ff0000;
"#;

const PNG_CHUNK_SIZE: usize = 0b1_000_000_000_000usize;
const USE_STREAMING_ITER: bool = false;

/// An error that actually print-prints the string
/// rather than doing what the standard string-to-error 
/// conversion stuff does which emits the control characters nya
#[derive(Eq, PartialEq, Clone, Ord, PartialOrd)]
pub struct PrettyError {
    errstr: String
}

impl From::<String> for PrettyError {
    fn from(d: String) -> Self {
        Self {
            errstr: d
        }
    }
}

/// This forces even main() to print out the
/// error nicely, since it uses std::fmt::Debug nya
impl std::fmt::Debug for PrettyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(self, f)
    }
}

impl std::fmt::Display for PrettyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // makes sure that the pretty error string
        // isn't prefixed/borked by the "Error:" prefix added by main nya
        write!(f, "\n{}", &self.errstr) 

    }
}

impl std::error::Error for PrettyError {}

/// Construct a parser error to display things nya
pub fn display_parser_error(
        input_str: &GraphemeClusterStr, 
        err: peg::error::ParseError::<usize>
    ) -> PrettyError {
    let mut res = String::with_capacity(1000);
    res.push_str(&format!(
        "PARSING FAILURE ~ Error at grapheme cluster {} in input file! NYA\n", 
        &err.location
    ));
    res.push_str("--- LOCATION ---\n");
    res.push_str(&format!("{}[|HERE|->]{}\n",
        GraphemeClusterStr::from(&input_str[(..err.location)]),
        GraphemeClusterStr::from(&input_str[(err.location..)])
    ));
    res.push_str("--- EXPECTED ONE OF ---\n");
    for token in err.expected.tokens() {
        res.push_str(&format!("{}", token));
    }
    PrettyError { 
        errstr: res
    }
}

/// Attempt to parse a fractal file from the input nya
///
/// Forwards various errors nya
fn try_parse(
    input_str: &mut impl std::io::Read
) -> Result::<
    fracfilev1parser::LinearisedSubstitution, 
    Box::<dyn std::error::Error + 'static>
> {
    let mut istr = String::with_capacity(1000);
    input_str.read_to_string(&mut istr)?;
    let graphemes: GraphemeClusterStr = istr.parse()?;
    let parse_res: Result::<
        fracfilev1parser::ValidatedSubs,
        _
    > = fracfilev1parser::grapheme_pattern_parser::ffv1(&graphemes);
    parse_res
        .map_err(|a| display_parser_error(&graphemes, a).into())
        .map(|a| fracfilev1parser::linearise_subs(a))
}

fn png_configuration(subs: &fracfilev1parser::LinearisedSubstitution) -> streamout::PaletteConfig {
    streamout::PaletteConfig::new(subs.palette_linearised_map()) 
}

/// Just checks the size of the output fits in a png ^.^
/// with a pretty error message nya
fn png_size_check(raw_xsize: usize, raw_ysize: usize) 
    -> Result::<(u32, u32), Box::<PrettyError>> {
    if raw_xsize > u32::MAX as usize {
        Err(format!("x output size too big for PNG: {}, max {}", 
            raw_xsize,
            u32::MAX
        )).map_err(PrettyError::from)?;
    }

    if raw_ysize > u32::MAX as usize {
        Err(format!("y output size too big for PNG: {}, max {}", 
            raw_ysize,
            u32::MAX
        )).map_err(PrettyError::from)?;
    }
    Ok((raw_xsize as u32, raw_ysize as u32))
}


/// Configures an encoder and dumps the header nya
fn configured_writer<T: std::io::Write>(
    png_cfg: &streamout::PaletteConfig,  
    outfile: T, 
    png_size: (u32, u32)
) -> Result::<png::Writer::<T>, Box::<dyn std::error::Error + 'static>> {
    Ok(png_cfg.configure(outfile, png_size.0, png_size.1).write_header()?)
}


/// Create an iterator for generating png bytes,
/// and also get the size of the output image nya
fn png_bytegen<'a>(
    png_cfg: &'a streamout::PaletteConfig, 
    subs: &'a fracfilev1parser::LinearisedSubstitution
) -> Result::<(
        (u32, u32), 
        impl Iterator::<Item=u8> + 'a 
    ), 
    Box::<dyn std::error::Error + 'static>
> {
    // this makes it work. ok. array orderings gimme a headache and this fixes the bug nya
    // (as opposed to C-order). Presumably it may have to do with me borking up generation
    // order or something nya, since i think it is *supposed* to be C-order, but eh nya.
    let generator = subs.make_pattern_generator(gen::FortranOrder::new(2));

    let (xsize, ysize) = png_size_check(
        generator.output_size()[0], 
        generator.output_size()[1]
    )?;

    // Build a generator nya
    let png_byte_generator = png_cfg.make_byte_iterator_for(
        generator.into_iter(),
        Some(xsize)
    );

    Ok(((xsize, ysize), png_byte_generator))
}

/// Actually spit out the png to the given configured writer nyaa
fn png_write<W: std::io::Write>(
    png_writer: &mut png::Writer::<W>,
    png_cfg: &streamout::PaletteConfig,
    png_byte_gen: impl Iterator::<Item=u8>,
    out_size: (u32, u32)
) -> Result::<(), Box::<dyn std::error::Error + 'static>> {
    let raw_max_bytes = png_cfg.max_bytes(out_size.0, out_size.1);
    // eprintln!("raw_uncompressed_bytes for png data: {}", raw_max_bytes);
    let mut buffer = Vec::<u8>::with_capacity(raw_max_bytes);
    buffer.extend(png_byte_gen);
    Ok(png_writer.write_image_data(&buffer)?)
}

fn generate(
    mut ffile_in: impl std::io::Read,
    png_out: impl std::io::Write
) -> Result::<(), Box::<dyn std::error::Error + 'static>> {
    let frac_subs = try_parse(&mut ffile_in)?; 
    let png_config = png_configuration(&frac_subs);
    let png_file = png_out;
    let ((xsize, ysize), bytegen) = png_bytegen(&png_config, &frac_subs)?;
    let mut png_writer = configured_writer(&png_config, png_file, (xsize, ysize))?;
    
    png_write(&mut png_writer, &png_config, bytegen, (xsize, ysize))
}


#[derive(StructOpt)]
#[structopt(name = "fractaliser4", author="sapient_cogbag (they/them) (lowercase only)")]
/// Fractaliser4 is a program for generating fractals via substituting states
/// with patterns of states.
enum OurArgs {
    /// Command to generate fractals nya
    Gen {
        /// The location to take the fractal data file from
        ///
        /// "-" indicates stdin, which is also the default if no arg provided nya
        #[structopt(long, short, default_value="-")]
        input: String,
        
        /// The location to produce the output fractal PNG file
        ///
        /// "-" indicates stdout, which is also the default if no arg provided
        #[structopt(long, short, default_value="-")]
        output: String
    },
    /// Prints an example of a fractaliser file (version 1) nya
    Example {},
    /// Checks the input for errors, reporting them on stderr nya
    Check {
        /// The location to take the fractal data file and
        /// scan for errors from nya.
        ///
        /// "-" indicates stdin, which is also the default if no argument
        /// is provided nya
        #[structopt(long, short, default_value="-")]
        input: String
    }
    
}



fn main() -> Result::<(), Box::<dyn std::error::Error + 'static>> {
   
    let arg = OurArgs::from_args();
    match arg {
        OurArgs::Gen {input, output} => {
            if input == "-" {
                let in_r = std::io::stdin();
                if output == "-" {
                    generate(in_r, std::io::stdout().lock())?;
                } else {
                    generate(in_r, std::fs::File::create(output)?)?;
                }    
            } else {
                let in_r = std::fs::File::open(input)?;
                if output == "-" {
                    generate(in_r, std::io::stdout().lock())?;
                } else {
                    generate(in_r, std::fs::File::create(output)?)?;
                }
            }
        },
        OurArgs::Example {} => {
            eprintln!("{}", GRAMMAR_EXAMPLE);
        },
        OurArgs::Check { input } => {
            if input == "-" {
                try_parse(&mut std::io::stdin())?;
            } else {
                try_parse(&mut std::fs::File::open(input)?)?;
            }
        }
    };

    Ok(())
}

/*
fractaliser4 - fractal generator
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
