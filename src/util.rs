#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct RGB {
    pub r: u8,
    pub g: u8,
    pub b: u8
}


/// Take the fields from a type in sequence as an iterator,
/// by creating an enum with all states and a done state nya
///
/// You can put init after the enum to implement IntoIterator
/// with an initial state too nyaaa
#[macro_export]
macro_rules! field_iterator {
    ($(#[$attrs:meta])* $vis:vis enum $iter_name:ident 
     for $og_type_name:ty : $field_type:ty $(where init $initial_state:ident)? {
    
        $(#[$final_docs:meta])*
        term $final_state:ident,
        $(
            $(#[$state_docs:meta])*
            cont $state:ident => $cont:ident $to_state:ident takes $field:ident
        ),*
    }) => {
        $(#[$attrs])*
        #[derive(Eq, PartialEq)]
        $vis enum $iter_name {$(
                $(#[$state_docs])*
                $state($og_type_name)
            ),* ,
            $(#[$final_docs])*
            $final_state
        }

        impl Iterator for $iter_name {
            type Item = $field_type;
            
            fn next(&mut self) -> Option::<Self::Item> {
                match self {
                    $(Self::$state(data) => {
                        let res = Some(data.$field.clone());
                        field_iterator!{$cont $to_state $field self Self : data}
                        res
                    }),*
                    Self::$final_state => None
                }
            }
        }

        $(impl IntoIterator for $og_type_name {
            type Item = $field_type;

            type IntoIter = $iter_name;
            
            fn into_iter(self) -> Self::IntoIter {
                <$iter_name>::$initial_state(self)
            }
        })?
    };

    // matchers for detecting if we need brackets or not to grab data nya
    (term $to_state:ident $field:ident $self:ident $self_ty:ty : $data:ident) => {
        *$self = <$self_ty>::$to_state;
    };

    (cont $to_state:ident $field:ident $self:ident $self_ty:ty : $data:ident) => {
        *$self = <$self_ty>::$to_state(*$data);
    };
}

field_iterator!{
    pub enum RGBIter for RGB : u8 where init R {
        term Done,
        cont R => cont G takes r,
        cont G => cont B takes g,
        cont B => term Done takes b
    }
}


#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct RGBA {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8
}

field_iterator!{
    pub enum RGBAIter for RGBA : u8 where init R {
        /// Terminal state nya
        term Done,
        cont R => cont G takes r,
        cont G => cont B takes g,
        cont B => cont A takes b,
        cont A => term Done takes a
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Colour {
    RGB(RGB),
    RGBA(RGBA)
}

impl Colour {
    /// Get this colour without its transparency nya
    pub fn discarded_transparency(&self) -> RGB {
        match self {
            Colour::RGB(rgb) => *rgb,
            Colour::RGBA(RGBA {r, g, b, a: _}) => RGB {r: *r, g: *g, b: *b}
        }
    }

    /// Get this colour with transparency, making the assumption that RGB colours
    /// are always full opacity nya
    pub fn with_transparency(&self) -> RGBA {
        match self {
            Colour::RGB(d) => d.as_rgba(),
            Colour::RGBA(d) => *d
        }
    }

    /// Get this colour with the given opacity if it doesn't already have opacity
    /// data (e.g. is RGB rather than RGBA nya)
    pub fn with_default_transparency(&self, opacity: u8) -> RGBA {
        match self {
            Colour::RGB(d) => d.with_opacity(opacity),
            Colour::RGBA(d) => *d
        }
    }
}


impl RGB {
    pub const fn with_opacity(&self, opacity: u8) -> RGBA {
        RGBA {
            r: self.r, g: self.g, b: self.b, a: opacity
        }
    }

    pub const fn as_rgba(&self) -> RGBA {
        self.with_opacity(255u8)
    }
}


impl From::<RGB> for RGBA {
    fn from(col: RGB) -> Self {
        col.as_rgba()
    }
}

impl From::<RGB> for Colour {
    fn from(data: RGB) -> Self {
        Self::RGB(data)
    }
}

impl From::<RGBA> for Colour {
    fn from(data: RGBA) -> Self {
        Self::RGBA(data)
    }
}


pub fn parse_hex_digit(ch: char) -> Option<Hex> {
    match ch {
        '0' => Some(Hex::D0),
        '1' => Some(Hex::D1),
        '2' => Some(Hex::D2),
        '3' => Some(Hex::D3),
        '4' => Some(Hex::D4),
        '5' => Some(Hex::D5),
        '6' => Some(Hex::D6),
        '7' => Some(Hex::D7),
        '8' => Some(Hex::D8),
        '9' => Some(Hex::D9),

        'A' => Some(Hex::DA),
        'B' => Some(Hex::DB),
        'C' => Some(Hex::DC),
        'D' => Some(Hex::DD),
        'E' => Some(Hex::DE),
        'F' => Some(Hex::DF),

        'a' => Some(Hex::DA),
        'b' => Some(Hex::DB),
        'c' => Some(Hex::DC),
        'd' => Some(Hex::DD),
        'e' => Some(Hex::DE),
        'f' => Some(Hex::DF),
        _ => None
    }
}

pub fn parse_decimal_digit(ch: char) -> Option<Dec> {
    match ch {
        '0' => Some(Dec::D0),
        '1' => Some(Dec::D1),
        '2' => Some(Dec::D2),
        '3' => Some(Dec::D3),
        '4' => Some(Dec::D4),
        '5' => Some(Dec::D5),
        '6' => Some(Dec::D6),
        '7' => Some(Dec::D7),
        '8' => Some(Dec::D8),
        '9' => Some(Dec::D9),
        _ => None
    }
}



#[macro_export]
macro_rules! maybe_pull_n_codepoints {
    ($take:literal, $from:expr) => { {
        let data: &str = $from;
        let mut taker = data.chars();
        let mut into = ['\0'; $take];
        // .nth() is a consuming function so we always pull #0 nya
        let did = into.iter_mut().map(
            |character| { *character = taker.nth(0)?; Some(()) }
            // make sure all characters actually got pulled and if 
            // so then we pull into into the data nya
        ).fold(Some(()), |sum, res| sum.and(res));
        did.map(move |_no| into)
    } }
}




/// This lets you hold either a vector OR a slice,
/// to represent a slice of things. This enables holding special references
/// nya
#[derive(Clone, Debug)]
pub enum ConceptualSlice<Item> {
    Vec(Vec::<Item>)
}


impl <Item> ConceptualSlice::<Item> {
    #[inline]
    pub fn as_slice(&self) -> &[Item] {
        match &self {
            ConceptualSlice::Vec(v) => {v.as_slice()}
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.as_slice().len()
    }

    #[inline]
    pub fn get<I: std::slice::SliceIndex::<[Item]>>(&self, index: I) 
        -> Option::<&I::Output> {
        self.as_slice().get(index)
    }
}

// when collecting we just pull from the iterator nya
impl <'slorig, Item> std::iter::FromIterator::<Item> 
for ConceptualSlice::<Item> {
    fn from_iter<T: IntoIterator<Item = Item>>(iter: T) -> Self {
        let vec: Vec::<Item> = iter.into_iter().collect();
        Self::Vec(vec)
    }
}

// Forward stuff like from std::Vec nyaa
impl <'slorig, Item, Idx: std::slice::SliceIndex::<[Item]>> std::ops::Index::<Idx> 
for ConceptualSlice::<Item> {
    type Output = Idx::Output;

    fn index(&self, index: Idx) -> &Self::Output {
        self.as_slice().index(index)
    }
}

// forward stuff from std::vec
impl <'a, 'slorig, Item> IntoIterator
for &'a ConceptualSlice::<Item> {
    // the conceptual slice can be owning nya
    // so we have to use the ref lifetime rather than 
    // the full lifetime in the case of a slice nya
    type Item = <&'a [Item] as IntoIterator>::Item;

    type IntoIter = <&'a [Item] as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.as_slice().into_iter()
    }
}

/// take a fixed # of elements from an iterator and dump into an array
#[macro_export]
macro_rules! maybe_take {
    (0, $from:expr) => {
        {
            let mut iter = $from;
            let first_item = iter.next();
            match first_item {
                Some(a) => Some([a;0]),
                None => None
            }
        }
    };
    ($n:literal, $from:expr) => {
        {
            let mut iter = $from;
            let first_item = iter.next();
            match first_item {
                None => None,
                Some(first) => { 
                    // We need something to fill in the array nya
                    let mut arr = [first;$n];
                    let mut had_none = false;
                    // fill the rest of the array ^.^
                    arr[1..].iter_mut().map(|current_data_pt| {
                        match iter.next() {
                            None => {had_none = true;},
                            Some(data) => {*current_data_pt = data;}
                        };
                    }).for_each(drop);
                    match had_none {
                        false => Some(arr),
                        true => None
                    }
                }
            }
        };
    }
}


/// Efficiently create a String from an array of chars (minimal allocation)
pub fn make_string_chars(chs: &[char]) -> String {
    let bytes = chs.iter().fold(0usize, |acc, elem| acc + elem.len_utf8());
    let mut our_str = String::with_capacity(bytes);
    for ch in chs {
        our_str.push(*ch);
    }
    our_str
}

/// Compare iterators elementwise. If there are non-matching lengths or there are
/// unequal elements anywhere, this returns false
pub fn iter_elem_eq<T, It1, It2> (a: &mut It1, b: &mut It2) -> bool
where T: PartialEq, It1: Iterator::<Item=T>, It2: Iterator::<Item=T> {
    loop {
        let (n1, n2) = (a.next(), b.next());
        match (n1, n2) {
            // Compare elems
            (Some(a_elem), Some(b_elem)) => {
                if a_elem != b_elem {
                    return false;
                }
            },
            // mismatched # of elems
            (Some(_a), None) => return false,
            (None, Some(_b)) => return false,
            // Matched # of elems, all elements matched (otherwise would have broken
            // with return earlier nya
            (None, None) => return true

        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Hex {
    D0 = 0,
    D1 = 1,
    D2 = 2,
    D3 = 3,
    D4 = 4,
    D5 = 5,
    D6 = 6,
    D7 = 7,
    D8 = 8,
    D9 = 9,
    DA = 10,
    DB = 11,
    DC = 12,
    DD = 13,
    DE = 14,
    DF = 15,
}

pub enum Dec {
    D0 = 0,
    D1 = 1,
    D2 = 2,
    D3 = 3,
    D4 = 4,
    D5 = 5,
    D6 = 6,
    D7 = 7,
    D8 = 8,
    D9 = 9,
}

use std::collections::{hash_map::HashMap, hash_set::HashSet};

/// Checks if the given hash map has keys for every element in the 
/// given hashset nya
pub fn has_keys_for_all_in<T: std::hash::Hash + Eq, Val>(
    map: &HashMap::<T, Val>, 
    keys: &HashSet::<T>
) -> bool {
    keys.iter().all(|key| map.contains_key(key))
}

/// VoidErr
///
/// A simple thing that implements Error but does nothing, intended simply
/// for cases where a function requires options for error outputs but there is no
/// need nya

#[derive(Eq, PartialEq, Clone, Copy, Default, Debug)]
pub struct VoidErr();

impl std::fmt::Display for VoidErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "VoidError")
    }
}

impl std::error::Error for VoidErr {}


/// An iterator that takes blocks of fixed length out of other 
/// iterators and spits out blocks of data terminated with markers
///
/// Internal data state representation nya
enum ChunkingIterInternal {
    Taking {
        remaining: usize
    },
    NoMore
}

pub enum ChunkingIterComponent <Item> {
    Data(Item),
    BlockMarker
}

/// An iterator that takes another iterator and splits it into chunks,
/// with a data block marker on the end nya
///
/// This is guarunteed not to produce empty blocks
/// (e.g. you always get a None if there are no more elements
/// left, even if the backing iterator happens to end before the end of the block nya
pub struct RawChunkingIter <It: Iterator>{
    original_iter: std::iter::Peekable::<It>,
    chunk_max_size: usize,
    current_state: ChunkingIterInternal
} 

impl <It: Iterator> RawChunkingIter::<It> {
    pub fn new(iter: It, max_chunk_size: usize) -> Self {
        Self {
            original_iter: iter.peekable(),
            chunk_max_size: max_chunk_size,
            current_state: ChunkingIterInternal::Taking {
                remaining: max_chunk_size
            }
        }
    }

    /// Get an iterator over the rest of the remaining chunk
    /// if there is any
    ///
    /// Note that taking from the iterator directly between `TakeFromSingleChunkIter`
    /// iterations may result in empty chunks being produced nya
    pub unsafe fn next_chunk_iter(&mut self) -> Option::<TakeSingleChunkIter::<'_, It>> {
         match self.current_state {
             ChunkingIterInternal::Taking { remaining: _ } => {
                Some(TakeSingleChunkIter::Taking(self))
             },
             ChunkingIterInternal::NoMore => None
         }
    }
}


impl <It: Iterator> Iterator for RawChunkingIter::<It> {
    type Item = ChunkingIterComponent::<It::Item>;

    fn next(&mut self) -> Option::<Self::Item> {

        // TODO: clean this code up with better data structuring, it's horrible nya
        match self.current_state {
            // This means the previous output was the blockmarker - If the next element of the
            // iterator is None then don't get anything nya
            ChunkingIterInternal::Taking { remaining } if remaining == self.chunk_max_size => {
                let next = self.original_iter.next();
                match next {
                    Some(actual_next) => {
                        self.current_state = ChunkingIterInternal::Taking { remaining: remaining - 1 };
                        Some(ChunkingIterComponent::Data(actual_next)) 
                    },
                    None => {
                        self.current_state = ChunkingIterInternal::NoMore;
                        None
                    }
                }
            },
            // end of block, emit as such :3
            // Keeping internal state consistent with a description of the data is harddddd nya
            // we can easily peek though to check if there's any more :3 nya
            // TODO: do some proper funky stuff with ChunkingIterInternal for representing the 
            // state without needing peek nya
            ChunkingIterInternal::Taking { remaining: 0 } => {
                // ensure that at the end of a chunk we always determine if there are any more nyaa
                match self.original_iter.peek() {
                    Some(_) => {self.current_state = ChunkingIterInternal::Taking { remaining: self.chunk_max_size }; },
                    None => {self.current_state = ChunkingIterInternal::NoMore;}
                }
                Some(ChunkingIterComponent::BlockMarker)
            },
            // this is after there were no emitted blocks so even if there is none left we want
            // to spit out a block marker nya (since there is nonzero length stuff involved ;3
            ChunkingIterInternal::Taking { remaining } => {
                let next = self.original_iter.next();
                match next {
                    Some(actual_next) => {
                        self.current_state = ChunkingIterInternal::Taking {remaining: remaining - 1 };
                        Some(ChunkingIterComponent::Data(actual_next))
                    },
                    // No more in internal iterator, spit out the block marker nya
                    None => {
                        self.current_state = ChunkingIterInternal::NoMore;
                        Some(ChunkingIterComponent::BlockMarker)
                    }
                }
            },
            ChunkingIterInternal::NoMore => None,
        }
    }
}


/// Take a single chunk from an iterator and iterate over that.
///
pub enum TakeSingleChunkIter <'a, OgIter: Iterator> {
    Taking (&'a mut RawChunkingIter::<OgIter>),
    Done
}

impl <'a, OgIter: Iterator> Iterator for TakeSingleChunkIter::<'a, OgIter> {
    type Item = OgIter::Item;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            TakeSingleChunkIter::Taking(chunking) => {
                match chunking.next() {
                    Some(ChunkingIterComponent::Data(item)) => Some(item),
                    Some(ChunkingIterComponent::BlockMarker) => {
                        *self = Self::Done;
                        None
                    }
                    None => None
                }
            },
            TakeSingleChunkIter::Done => None
        }
    }
}


/// A properly containerised iterator over chunk iterators :) nya
///
/// Lets you work on max-length chunks out of an iterator easily nya, in particular
/// by using structures like: 
/// ```
/// while let Some(chunk_iter) = your_chunk_iter.next_chunk() {
///     ....
/// }
///
/// This structure can be created for any iterator nya, as long as the chunk size
/// is nonzero
pub struct ChunkIter <OgIter: Iterator> {
    raw_chunk_iter: RawChunkingIter::<OgIter>
}

/// This takes iterator chunks and parses them back into iterator-land nya,
/// allowing for processing on an individual chunk
///
/// HRTBs ftw nya
pub struct ChunkReducer <OutItem, OgIter, ProcessorFn> 
where 
    for <'a> ProcessorFn: FnMut(TakeSingleChunkIter::<'a, OgIter>) -> OutItem,
    OgIter: Iterator

{
    parser: ProcessorFn,
    chunker: ChunkIter::<OgIter>
}


// impl Iterator for ChunkReducer nya
//
// all of these bounds are just for the struct to have those as input types >.<
// nya
impl <OutItem, OgIter: Iterator, ProcessorFn> Iterator 
for ChunkReducer::<OutItem, OgIter, ProcessorFn> 
where for <'a> ProcessorFn: FnMut(TakeSingleChunkIter::<'a, OgIter>) -> OutItem {
    type Item = OutItem;

    fn next(&mut self) -> Option<Self::Item> {
        let next_chunk = self.chunker.next_chunk();
        match next_chunk {
            Some(chunk) => {Some((self.parser)(chunk))}
            None => {None}
        }
    }
}

impl <OgIter: Iterator> ChunkIter::<OgIter> {
    pub fn next_chunk(&mut self) -> Option::<TakeSingleChunkIter::<'_, OgIter>> {
        // SAFETY: this field is entirely private so users can't screw up the raw chunk iterator :3
        // nyaa
        unsafe { self.raw_chunk_iter.next_chunk_iter() }
    }

    /// Convert this into an iterator that processes each chunk and spits
    /// out a single result nya
    pub fn per_chunk_process<OutItem, Processor>(
        self, 
        chunk_processor: Processor
    ) -> ChunkReducer::<OutItem, OgIter, Processor>
        where for <'a> Processor: FnMut(TakeSingleChunkIter::<'a, OgIter>) -> OutItem {
        ChunkReducer {
            parser: chunk_processor,
            chunker: self
        }
    }
}


pub trait ChunkIterExtension : Iterator + Sized {
    /// Convert an iterator into non-empty chunks separated by block markers, of maximum length
    /// that passed into this function
    ///
    /// The size must be nonzero nya
    ///
    /// This converts an `Iterator <Item>` into an `Iterator <ChunkingIterComponent <Item> >`
    /// which produces ChunkingIterComponent::Data(Item) to indicate a current chunk and
    /// ChunkingIterComponent::BlockMarker to indicate the end of a chunk nya. It is guarunteed
    /// not to produce ChunkingIterComponent::BlockMarker s adjacent to each other - i.e. there
    /// are no empty produced chunks nya
    ///
    /// Trying to make a plain iterator-of-iterators causes intractible lifetime problems
    /// unfortunately, that as far as i can tell are incompatible with the Iterator trait since 
    /// each produced iteration needs a mutable reference to the parent iterator nya
    unsafe fn as_raw_chunked(self, max_chunk_size: usize) -> RawChunkingIter::<Self>;

    /// Convert an iterator into an object that spits out iterators over chunks
    /// of maximum length given, that are always nonempty chunks nya
    fn chunked(self, max_chunk_size: std::num::NonZeroUsize) -> ChunkIter::<Self> {
        ChunkIter {
            // SAFETY: we immediately contain the raw chunks into a struct controlling access to
            // them nya, preventing user borkage of the chunk generator. We also ensure the size
            // is nonzero nya
            raw_chunk_iter: unsafe {self.as_raw_chunked(max_chunk_size.get())}
        }
    }
}

impl <T: Iterator + Sized> ChunkIterExtension for T {
    unsafe fn as_raw_chunked(self, max_chunk_size: usize) -> RawChunkingIter::<Self> {
        RawChunkingIter::new(self, max_chunk_size)
    }
}



/// Macro for making combo iterators :3 nyaa
#[macro_export]
macro_rules! multi_iterator {
    ($(#[$attrs:meta])*
    $vis:vis enum $miter_name:ident <> {
        $(#[$first_attrs:meta])*
        $first_var:ident ($first:ident),
        $(
        $(#[$rest_attrs:meta])*
        $rest_var:ident ($rest:ident)
        ),+
    }) => {
        $(#[$attrs])*
        $vis enum $miter_name <
            $first: ::std::iter::Iterator, 
            $($rest: ::std::iter::Iterator::<Item=<$first>::Item>),+
        > {
            $(#[$first_attrs])*
            $first_var ($first),
            $(
            $(#[$rest_attrs])*
            $rest_var ($rest)
            ),+
        }

        impl <
            $first: ::std::iter::Iterator,
            $($rest: ::std::iter::Iterator::<Item=$first::Item>),+
        > ::std::iter::Iterator for $miter_name <$first, $($rest),+> {
            type Item = $first::Item;

            fn next(&mut self) -> Option::<Self::Item> {
                match self {
                    $miter_name::$first_var(it) => it.next(),
                    $($miter_name::$rest_var(it) => it.next()),+
                }
            }
        }
    }
}


multi_iterator! {
    /// Iterator combining three other iterators nya
    pub enum TriIterator <> {
        Left(It1),
        Middle(It2),
        Right(It3)
    }
}

multi_iterator! {
    /// Iterator combining four other iterators ^.^
    pub enum QuadIterator <> {
        L1(It1),
        L2(It2),
        L3(It3),
        L4(It4)
    }
}



multi_iterator! {
    /// A little thing to produce one or the other iterator :3 nyaa
    /// Avoids boxing ^.^
    ///
    pub enum BiIterator <> {
        Left(It1),
        Right(It2)
    }
}





/*
fractaliser4 - fractal generator
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
