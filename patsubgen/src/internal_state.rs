use ndarray::prelude::*;
use std::collections::{HashSet, HashMap};
use crate::err;
use std::iter::Iterator;


pub type InternalStateType = usize;

/// Internal representation of a state as an integer nyaa
///
/// This allows for much more efficient substitution nya, rather than using a hashmap you can just
/// index into a vector
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Default)]
pub struct InternalState(pub InternalStateType);

impl InternalState {
    pub fn new(state: &InternalStateType) -> Self {
        Self(*state)
    }

    pub fn get(&self) -> &InternalStateType {
        &self.0
    }
}

/// Internal representation nutype of the number of integer states available to represent a bunch
/// of user states nyaaa
///
/// When a pile of user states is wanted to get used in a pattern substitution, they are enumerated
/// beforehand.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Default)]
pub struct InternalStateCount(pub usize);

impl InternalStateCount {
    pub fn get(&self) -> &usize {
        &self.0
    }
}


/// A pattern size nyaa.
///
/// Represents the size of an internal state pattern.
#[derive(Clone, Debug)]
pub struct PatternSize <Dim: Dimension> {
    size: Dim,
    ndim: usize
}

impl <Dim: Dimension> PatternSize <Dim> {
    /// Get a pattern size from an actual pattern
    pub fn new(pattern: &Pattern::<Dim>) -> Self {
        let size = pattern.view().raw_dim();
        let ndim = size.ndim();
        Self {
            size,
            ndim
        }
    }

    /// Make a pattern size directly nya
    pub fn from_size(size: Dim) -> Self {
        let ndim = size.ndim();
        Self {
            ndim,
            size
        }
    }

    /// Get the number of dimensions
    pub fn ndim(&self) -> usize {
        self.ndim
    }

    /// Get the raw size as a dim
    pub fn raw_dim(&self) -> &Dim {
        &self.size
    }
}

impl <DimUs: Dimension, DimThem: Dimension> PartialEq::<PatternSize::<DimThem>> 
for PatternSize::<DimUs> {
    fn eq(&self, other: &PatternSize::<DimThem>) -> bool {
        self.raw_dim().slice() == other.raw_dim().slice() 
    }
}

/// An internal state pattern of the given dimensionality
#[derive(Clone, Debug)]
pub struct Pattern <Dim: Dimension> {
    pattern: Array::<InternalState, Dim>
}

impl <Dim: Dimension> Pattern <Dim> {
    /// Create from a raw internal state array nya
    pub fn new(pattern: Array::<InternalState, Dim>) -> Self {
        Self {pattern}
    }
        
    /// Obtain a view on the pattern nya
    pub fn view (&self) -> ArrayView::<'_, InternalState, Dim> {
        self.pattern.view() 
    }

    /// Obtain a reference to the raw array of the pattern nya
    pub fn raw_array (&self) -> &Array::<InternalState, Dim> {
        &self.pattern
    }

    /// Get the size of the pattern nya
    pub fn size(&self) -> PatternSize::<Dim> {
        PatternSize::new(self)
    }
}


/// Trait for having user-controlled pattern substitution systems for internal states
/// that uses indices to indicate input states for a given substitution nya, and requires
/// that the states be sequentially complete (i.e. there are no "gaps" from [0..state count) )
///
/// TODO: when rust allows `impl` restrictions on return values for functions in traits (non
/// generic functions i do not believe this would generate higher kinded types nya), fix this to
/// not require a whole trait lifetime but instead just one on the &'a self parameter nyaaa
pub trait PatternSubstitution {
    /// The dimension type of the patterns to substitute nya
    /// In practise SDim is 'static so the lifetime bound always works nya
    /// Or i think anyway...
    type SDim: Dimension;

    // we currently lack means to do this so we need to use (ew) dyn Trait nya
    // Iterator over all currently present patterns nyaaeow
    // type IntoIter: Iterator::<Item=InternalState>;


    /// Obtain the total number of input states currrently held by the pattern 
    /// substitution nyaa. The sequential completeness of the substitutions means
    /// that this should be the only thing needed to determine all the substitutions
    /// inside this mapping nya
    fn total_internal_states(&self) -> InternalStateCount;

    /// Get the substitution for the given index ;p
    /// Invalid substitution indices can spit out an error nya
    fn get_substitution_for(&self, internal_state: InternalState) 
        -> Result <&Pattern::<Self::SDim>, err::SubstitutionAccessErr>;

    /// Get the allowed/valid pattern size for elements in this substitution.
    fn pattern_size(&self) -> &PatternSize::<Self::SDim>;

    /// Iterator over all current states in the substitution.
    ///
    /// when called with `Self::get_substitution_for` this should not produce any
    /// error nyaa
    fn iterify(&self) -> Box::<dyn Iterator::<Item=InternalState>>;

}



/// Simple object for building an internal state substitution nyaa
#[derive(Debug, Clone)]
pub struct SubstitutionVBased <Dim: Dimension> {
    associations: Vec::<Pattern::<Dim>>,
    pattern_size: PatternSize::<Dim>

}

impl <Dim: Dimension> SubstitutionVBased <Dim> {
    /// Create a new substitution system of patterns of the given size nya
    pub fn new(substitution_size: PatternSize::<Dim>) -> Self {
        Self {
            associations: Vec::default(),
            pattern_size: substitution_size
        }
    }

    /// Create a new substitution system with patterns of the given size, with a hint for the # of
    /// states likely to be added nya
    pub fn with_states_hint(
        substitution_size: PatternSize::<Dim>, 
        expected_states: InternalStateCount
    ) -> Self {
        Self {
            associations: Vec::with_capacity(expected_states.0),
            pattern_size: substitution_size 
        }
    }

    /// Get an iterator over the states in this representation of the substitution
    /// nyaa
    ///
    /// Until we get HKTs we need to use boxes :( nya
    fn iter_over_states(&self) -> /*impl Iterator::<Item=InternalState>*/ Box::<dyn Iterator::<Item=InternalState>> {
        Box::new((0..self.total_internal_states().0).map(|a| InternalState::new(&a)))
    }
}



impl <Dim: Dimension> PatternSubstitution for SubstitutionVBased <Dim> { 
    type SDim = Dim; 

    fn total_internal_states(&self) -> InternalStateCount {
        InternalStateCount(self.associations.len())
    }

    fn get_substitution_for(&self, internal_state: InternalState) -> Result<&Pattern::<Dim>, err::SubstitutionAccessErr> {
        self.associations.get(internal_state.0)
            .ok_or(err::SubstitutionAccessErr::new(internal_state, self.total_internal_states()))
    }

    fn pattern_size(&self) -> &PatternSize::<Dim> {
        &self.pattern_size
    }
    

    fn iterify(&self) -> /*impl Iterator::<Item=InternalState>*/ Box::<dyn Iterator::<Item=InternalState>> {
        self.iter_over_states()
    }
    
}

/// Holds output states of a single internal state substitution pattern
#[derive(Debug, Clone)]
pub struct PatternOutputStates (HashSet::<InternalState>);

impl PatternOutputStates {
    /// Get the output states from a pattern
    pub fn from_pattern <Dim: Dimension> (pat: &Pattern::<Dim>) -> Self {
        Self (pat.view().iter().copied().collect())
    }

    pub fn get(&self) -> &HashSet::<InternalState> {
        &self.0
    }
}

impl IntoIterator for PatternOutputStates {
    type Item = <HashSet::<InternalState> as IntoIterator>::Item;
    type IntoIter = <HashSet::<InternalState> as IntoIterator>::IntoIter;
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}


/// Holds the output states of an entire substituition system nya
///
/// It is a mapping.
#[derive(Debug, Clone)]
pub struct SubsOutputStates(HashMap::<InternalState, PatternOutputStates>);

impl SubsOutputStates {
    pub fn from_substitution <T: PatternSubstitution>(subs: &T) -> Self {
        Self(
            subs.iterify()
            .map(|state| (state, subs.get_substitution_for(state).expect(
                "Someone incorrectly implemented the InternalStatePatternSubstitution API"
            )))
            .map(|(state, out)| (state, PatternOutputStates::from_pattern(out)))
            // convert into a map
            .collect()
        )
    }

    pub fn get(&self) -> &HashMap::<InternalState, PatternOutputStates> {
        &self.0
    }
}

/// Holds the input states of an entire substitution
#[derive(Debug, Clone)]
pub struct SubsInputStates(HashSet::<InternalState>);

impl SubsInputStates {
    pub fn from_substitution <T:PatternSubstitution>(subs: &T) -> Self {
        Self(subs.iterify().collect())
    }

    pub fn get(&self) -> &HashSet::<InternalState> {
        &self.0
    }
}

/// Holds the excess/not-in-input states for a given pattern
#[derive(Clone, Debug)]
pub struct PatternExcessStates(HashSet::<InternalState>);

impl PatternExcessStates {
    pub fn from_input_and_pattern_states(
        valid_input_states: &SubsInputStates,
        output_states: &PatternOutputStates
    ) -> Self {
        Self(output_states.0.difference(&valid_input_states.0).copied().collect())
    }

    pub fn from_input_and_pattern <Dim: Dimension> (
        valid_input_states: &SubsInputStates,
        pattern: &Pattern::<Dim>
    ) -> Self {
        Self::from_input_and_pattern_states(
            valid_input_states,
            &PatternOutputStates::from_pattern(pattern)
        )
    }

    pub fn get(&self) -> &HashSet::<InternalState> {
        &self.0
    }

    /// If this excess states object would cause a 
    /// pattern substitution to be self-inconsistent
    /// i.e. there is a nonzero amount of excess states nyaa
    pub fn breaks_self_consistency(&self) -> bool {
        self.0.len() != 0
    }
}

/// Holds the excess states of an entire substitution,
/// with mappings from states to their excess states.
///
/// Note that this automatically filters states with no excess
/// out of the map nyaa
/// nya
#[derive(Clone, Debug)]
pub struct SubsExcessStates(
    HashMap::<InternalState, PatternExcessStates>
);

impl SubsExcessStates {
    pub fn from_input_and_substitution_states(
        valid_input_states: &SubsInputStates,
        output_states: &SubsOutputStates
    ) -> Self {
        Self(
            output_states.0.iter()
            .map(|(instate, outstate)| {
                (instate.clone(), 
                 PatternExcessStates::from_input_and_pattern_states(
                    valid_input_states,
                    outstate
                ))
            })
            .filter(|(_instate, excess_states)| excess_states.breaks_self_consistency())
            .collect()
        )
    }

    pub fn from_substitution (subs: &impl PatternSubstitution) -> Self {
        Self::from_input_and_substitution_states(
            &SubsInputStates::from_substitution(subs),
            &SubsOutputStates::from_substitution(subs)
        )
    }

    /// Checks if this set of excess states breaks self - consistency nya
    /// i.e. there are no excess states nya. 
    pub fn breaks_self_consistency(&self) -> bool {
        self.0.iter().any(|(_, excess)| excess.breaks_self_consistency())
    }

    pub fn get(&self) -> &HashMap::<InternalState, PatternExcessStates> {
        &self.0
    }
}


/// States beyond a given (0..total_internal_states()) range 
#[derive(Debug, Clone)]
pub struct InvalidExcessStates(HashSet::<InternalState>);

impl InvalidExcessStates {
    pub fn new(
        expected_ub: &InternalStateCount, 
        istates: &SubsInputStates
    ) -> Self {
        Self(istates.get().iter()
            .filter(|x| x.0 >= expected_ub.0)
            .copied()
            .collect()
        )
    }

    pub fn get(&self) -> &HashSet::<InternalState> {
        &self.0
    }

    /// checks if this indicates invalid substitution nya
    ///
    /// any out of range ones do, so this is just a length check
    pub fn indicates_invalid_subs(&self) -> bool {
        self.0.len() != 0
    }
}

/// Holes in the present input states as compared to 
/// the expected integer states (0..total_internal_states())
#[derive(Debug, Clone)]
pub struct MissingInputStates(HashSet::<InternalState>);

impl MissingInputStates {
    pub fn new(
        expected_ub: &InternalStateCount,
        istates: &SubsInputStates
    ) -> Self {
        // select states not in the available input states nya
        Self((0..*expected_ub.get())
             .map(|x| InternalState::new(&x))
             .filter(|x| !(istates.get().contains(x)))
             .collect()
        )
    }

    pub fn get(&self) -> &HashSet::<InternalState> {
        &self.0
    }

    /// If this object when created implies that the substitution it is created from is invalid
    /// in terms of input states nya
    pub fn indicates_invalid_subs(&self) -> bool {
        self.0.len() != 0
    }
}

/// Holds sequential-zeroint-discontinuities in input states
/// i.e. states that are not in the range (0..total_internal_states())
/// and holes in that range (which don't have patterns available)
#[derive(Debug, Clone)]
pub struct StateSeqDeviations {
    holes: MissingInputStates,
    out_of_range: InvalidExcessStates
}

impl StateSeqDeviations {
    pub fn new(
        expected_state_count: &InternalStateCount, 
        input_states: &SubsInputStates
    ) -> Self {
        Self {
            holes: MissingInputStates::new(expected_state_count, input_states),
            out_of_range: InvalidExcessStates::new(expected_state_count, input_states)
        }
    }
}


/// Tags a pattern substitution as being valid.
///
/// VALIDITY CHECKS:
/// * This type ensures that all states from (0..state count) have associated substitution
///   patterns, and that no other states outside of that range exist
/// * This type ensures that all substitutions are self-consistent - i.e. there are no states
///   in the output of the substitutions that are not present in the inputs nyaa
pub struct ValidatedPatternSubstitution <ISPS: PatternSubstitution> {
    validated_state: ISPS 
}


impl <ISPS: PatternSubstitution> ValidatedPatternSubstitution <ISPS> {
    /// Checks a state for internal self consistency errors and state-integer
    /// completeness
    pub fn new_or_err(current_state: ISPS) -> Result <
        Self, 
        err::InternalConsistencyErr::<ISPS>
    > {
        let input_states = SubsInputStates::from_substitution(&current_state);
        let associated_output_states = SubsOutputStates::from_substitution(&current_state);
        let num_states = current_state.total_internal_states();

        // Check for unprocessed output states nya
        let self_inconsistencies = 
            SubsExcessStates::from_input_and_substitution_states(
                &input_states, 
                &associated_output_states
        );
        if self_inconsistencies.breaks_self_consistency() {
            return Err(err::InternalConsistencyErr::InputStateOpenTransform {
                subs: current_state, 
                excess_states: self_inconsistencies
            });
        }

        // check for holes and excess states
        let int_discont = MissingInputStates::new(&num_states, &input_states);
        if int_discont.indicates_invalid_subs() {
            return Err(err::InternalConsistencyErr::InputStateDiscontinuity {
                subs: current_state,
                discontinuity: int_discont
            });
        };

        Ok(Self {
            validated_state: current_state
        })
    }
    
    pub fn get(&self) -> &ISPS {
        &self.validated_state
    }

    pub fn pattern_size(&self) -> &PatternSize::<ISPS::SDim> {
        self.get().pattern_size()
    }

    pub fn state_count(&self) -> InternalStateCount {
        self.get().total_internal_states()
    }
}

impl <ISPS: PatternSubstitution> std::ops::Index::<InternalState> 
for ValidatedPatternSubstitution::<ISPS> {
    type Output = Pattern::<ISPS::SDim>;

    fn index(&self, index: InternalState) -> &Self::Output {
        self.get()
            .get_substitution_for(index)
            .expect("The type should validate this nya")
    }
}

/// Get an iterator over the states in a validated substitution and 
/// the patterns associated with them nyaa
pub fn sequential_substitution_iterator <ISPS: PatternSubstitution>(
    state: &ValidatedPatternSubstitution::<ISPS>
) -> impl Iterator::<Item = (InternalState, &Pattern::<ISPS::SDim>)> + '_ {
    (0..state.state_count().0).map(move |raw_statenum| {
        return (
            InternalState::new(&raw_statenum),
            &state[InternalState::new(&raw_statenum)]
        )
    })
}


/// We allow for patterns to exist in the form of vector-pattern-size pairs nya
/// This is essentially the purest distillation of state mapping - a pattern size
/// and a vector of arrays nya
impl <Dim: Dimension> PatternSubstitution 
for (Vec::<Pattern::<Dim>>, PatternSize::<Dim>) {
    type SDim = Dim;

    fn total_internal_states(&self) -> InternalStateCount {
        InternalStateCount(self.0.len())
    }

    fn get_substitution_for(&self, internal_state: InternalState) 
        -> Result<&Pattern::<Self::SDim>, err::SubstitutionAccessErr> {
        self.0.get(internal_state.0).ok_or(err::SubstitutionAccessErr::new(
            internal_state, self.total_internal_states()
        ))
    }

    fn pattern_size(&self) -> &PatternSize::<Self::SDim> {
        &self.1
    }

    fn iterify(&self) -> Box::<dyn Iterator::<Item=InternalState>> {
        Box::new((0..self.total_internal_states().0).map(|d| InternalState(d)))
    }
}



/*
patsubgen - multidimensional pattern substitution
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
