use crate::internal_state::{
    ValidatedPatternSubstitution, 
    PatternSubstitution, 
    InternalState, 
    InternalStateCount,
    PatternSize,
    sequential_substitution_iterator
};
use crate::ndarray::prelude::*;

/// Structure holding a finalised internal state substitution system for 
/// actually substituting nya
///
/// SubstitutionDim is the dimension of the substitution patterns.
pub struct Substitutions <SubstitutionDim: Dimension> {
    /// The substitutions - first axis is the sequential internal state
    /// to be substituted.
    subs: Array <InternalState, SubstitutionDim::Larger>,
    /// # of internal states nya
    available_states: InternalStateCount,
    /// Just the size of the patterns:
    pattern_size: PatternSize::<SubstitutionDim>
}

impl <Dim: Dimension> Substitutions::<Dim> {
    /// Solidify a validated substitution of states into a ndarray 
    /// fixed size array with axis-0 being the numerical id of the given
    /// state nyaa (and the rest of the axes are just the patterns ;p)
    ///
    pub fn new <ISPS: PatternSubstitution::<SDim=Dim>> (
        validated_subs: &ValidatedPatternSubstitution::<ISPS>
    ) -> Self {
        let num_of_states = validated_subs.state_count();
        let mut patterns_first_axis = validated_subs
            .pattern_size()
            .raw_dim()
            .insert_axis(Axis(0));
        
        patterns_first_axis[0] = num_of_states.0;
        
        let mut our_data = Self {
            available_states: num_of_states,
            subs: Array::default(patterns_first_axis),
            pattern_size: validated_subs.pattern_size().clone()
        };

        for (istate, substitution) in sequential_substitution_iterator(&validated_subs) {
            our_data.subs
                .index_axis_mut(Axis(0), istate.0)
                .assign(&substitution.view());
        };

        our_data
    }

    pub fn pattern_size(&self) -> &PatternSize::<Dim> {
        &self.pattern_size
    }


    /// Implement indexing to get an appropriate pattern nya
    pub fn subs_for(&self, index: InternalState) -> ArrayView::<
        '_, InternalState, <<Dim as Dimension>::Larger as Dimension>::Smaller
    > {
        self.subs.index_axis(Axis(0), index.0) 
    }
}


/// Module for generating sequences of substituted patterns nya
pub mod gen {
    use crate::internal_state;
    use crate::err;
    use super::{
        InternalState, 
        InternalStateCount, 
        PatternSize, 
        PatternSubstitution,
        Substitutions
    };
    use ndarray::prelude::*;

    /// Provides information about which axis to move along next if the end of
    /// the current axis is reached in incrementing based on raw memory location nya
    #[derive(Clone, Debug, Copy)]
    pub enum NextAxisIncr {
        /// There are no more axes after this one that should be incremented to move
        /// along in memory
        NoMore,
        /// The next axis to start moving along if we reached the end of this one 
        /// and reset it, to move along in memory nya
        NextAxis(Axis)
    }

    /// Represents a memory ordering - in particular it takes an axis that has been
    /// fully generated out and spits out the next axis to increment nya
    pub trait GenMemoryOrder {
        /// Get the next axis to move along to continue going along in memory, in the
        /// case that the current axis has reached the end nya
        fn next_axis(&self, axis: Axis) -> NextAxisIncr;

        /// The first axis to move along to move along in memory (i.e. the smallest 
        /// stride axis ^.^)
        fn first_axis(&self) -> Axis;
    }

    /// C Memory Order nya
    #[derive(Copy, Debug, Clone)]
    pub struct COrder {
        /// # dimensions in the array 
        /// Axes are Axis(0) ... Axis(ndim - 1)
        pub ndim: usize 
    }

    impl COrder {
        pub fn new(ndim: usize) -> Self {
            Self {ndim}
        }
    }

    /// Fortran Memory Order nya
    #[derive(Copy, Debug, Clone)]
    pub struct FortranOrder {
        /// # dimensions in the array nya
        /// Axes are Axis(0) ... Axis(ndim - 1)
        pub ndim: usize
    }

    impl FortranOrder {
        pub fn new(ndim: usize) -> Self {
            Self {ndim}
        }
    }


    /// Corder has the largest axis with the smallest stride 
    impl GenMemoryOrder for COrder {
        fn next_axis(&self, axis: Axis) -> NextAxisIncr {
            match axis {
                Axis(0) => NextAxisIncr::NoMore,
                Axis(n) => NextAxisIncr::NextAxis(Axis(n - 1))
            }
        }

        fn first_axis(&self) -> Axis {
            Axis(self.ndim - 1)
        }
    }

    /// FortranOrder has the smallest axis with the smallest stride nya
    impl GenMemoryOrder for FortranOrder {
        fn next_axis(&self, axis: Axis) -> NextAxisIncr {
            match axis {
                Axis(n) if n == self.ndim - 1 => NextAxisIncr::NoMore,
                Axis(n) => NextAxisIncr::NextAxis(Axis(n + 1))
            }
        }

        fn first_axis(&self) -> Axis {
            Axis(0)
        }
    }


    /// Holds a pattern generator from some input data and substitution
    pub struct PatternGenerator <
        'inarr, 
        'insubs, 
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > {
        input: ArrayView::<'inarr, InternalState, InSize>,
        substitutions: &'insubs Substitutions::<SubDim>,
        /// number of substitutions to make nya
        substitution_depth: usize,
        /// The order in which elements are generated
        generation_order: MemoryOrder,
        /// Size of the patterns
        pattern_size: PatternSize::<SubDim>,
        /// Dimensions of the output
        output_dimensions: InSize,
        /// relevent number of dimensions we have nya
        ndim: usize,
        /// Number of output elements nya
        total_output_size: usize 
    }

    impl <
        'inarr, 
        'insubs, 
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > PatternGenerator::<'inarr, 'insubs, SubDim, InSize, MemoryOrder> {
        pub fn new_or_err(
            input: ArrayView::<'inarr, InternalState, InSize>,
            substitutions: &'insubs Substitutions::<SubDim>,
            subdepth: usize,
            generation_order: MemoryOrder
        ) -> Result::<Self, err::InputNDimErr> {
            err::InputNDimErr::new_err(
                &input.raw_dim(), 
                substitutions.pattern_size().raw_dim()
            )?;
            
            let pattern_size = substitutions.pattern_size();
            let ndim = pattern_size.ndim();
            
            // we know that the ndims of the dimensions match so we can only 
            // match patterns on one nya ^.^
            let output_dimensions: InSize = {
                let mut outdims = InSize::zeros(ndim);
                for axis in 0..ndim {
                    outdims[axis] = 
                        input.raw_dim()[axis] * pattern_size.raw_dim()[axis].pow(
                            subdepth as u32
                        )
                };
                outdims
            };

            let total_output_size = output_dimensions.size();
            
            Ok(Self {
                input,
                substitutions,
                substitution_depth: subdepth,
                generation_order,
                pattern_size: pattern_size.clone(),
                ndim,
                output_dimensions,
                total_output_size
            })
        }

        pub fn output_size(&self) -> &InSize {
            &self.output_dimensions
        }
    }


    /// Represents the status after trying to increment a given axis.
    #[derive(Clone, Debug, Copy)]
    pub enum AxisIncrStatus {
        Fine,
        Overflow
    }

    /// Represents the status after trying to increment an entire pattern
    /// location nya
    #[derive(Clone, Debug, Copy)]
    pub enum PatLocIncrStatus {
        // Incremented fine
        Fine,
        // no more locations available after this nya
        NoMoreLocations
    }

    /// A substitution location specified in such a way that 
    /// it can easily be used to get the repeatedly-substituted value in
    /// a set of patterns, in a substituted output nya
    ///
    /// ALGORITHM and CONCEPTUALISATION nya ~
    ///
    ///  * We want to do *n* operations of taking a value and replacing it with a pattern nya
    ///
    ///  * We have a pattern of size (P1x...xPk)
    ///
    ///  * The output space for an input of (1x1x...x1) size is ((P1)^n x ... x (Pk)^n)
    ///    since every element is replaced by one of ((P1) x ... x (Pk)) when substituted nya
    ///
    ///  * To calculate the state of the final cell, you need to know the intermediate substitutions.
    ///    as well as the subcoordinate within that that eventually gets substituted again and produces
    ///    the required value for this cell nya
    ///
    ///  * All cells within each (P1 x...x Pk) region where these are created by differing modulos (per-axis) of (P1 x...x Pk)
    ///    are generated from an associated single cell from the previous iteration nya, where this is
    ///    located in the previous substitution iteration at 
    ///    (current_location - current_location mod (P1 x...x Pk))/(P1 x...x Pk), with the location
    ///    relative to the substituted subregion of the output simply being current_location % (P1 x
    ///    ... x Pk)
    ///
    ///  * As such locating the indices in each level essentially is the same problem as converting the
    ///    axis(i) locations into their numerical representations in base-(Pi)
    ///  
    /// Note on C-order vs F-order. C order means that int i[A][B] is like (int i[B])[A] in terms of
    /// memory order nya [see](https://en.wikipedia.org/wiki/Row-_and_column-major_order), but i[a][b]
    /// for accessing nya, which is like i[B * a + b]
    ///
    /// F-order means that int i[A][B] is like (int i[A])[B], and i[a][b] is like i[A * b + a]
    ///
    /// We use this for efficient accesses and in generation of pattern-substituted data nyaa
    ///
    /// For this nyaa, we have the number of dimensions be (depth of substitution, # of axes)
    /// since we will co-access all the things in a given depth at once nya, so having them close 
    /// in memory is good nya
    ///
    /// We need to do a test to see if this is more or less efficient than doing modulo-division things
    /// on a fixed number nyaa
    #[derive(Clone, Debug)]
    pub struct SubPatsLocation <LocationType: Dimension> {
        /// the current location as an array where each a[axis] is one axis in the 
        /// output, and each element other than the first is a number in [0, (pattern size
        /// in that dimension-1]. The first is the actual location in the input nya
        current_location: Array2::<usize>,
        /// How many times to pattern-substitute nya
        /// - this + 1 is the number of columns? (axis-0 dimension) in the array)
        subdepth: usize,
        /// The number of dimensions in the location nya (this is the axis-0? size)
        dimensions: usize,
        // this is used to indicate that we essentially have a block of LocationTypes
        // in some abstract sense nya (note the ptr indicates nonownership for
        // rusts drop check, see: https://doc.rust-lang.org/std/marker/struct.PhantomData.html
        _a: std::marker::PhantomData::<*const LocationType>
    }

    impl SubPatsLocation::<IxDyn> {
        /// Create a subpatloc of dynamic dimensionality.
        pub fn zeroed_dynamic(substitution_depth: usize, io_dimensionality: usize) -> Self { 
            Self {
                current_location: Array2::zeros(Ix2(
                    substitution_depth + 1,
                    io_dimensionality
                )),
                subdepth: substitution_depth,
                dimensions: io_dimensionality,
                _a: Default::default()
            }
        }
    }

    /// TODO: Add constraint to disallow Loc::NDIM = None nya
    impl <Loc: Dimension> SubPatsLocation::<Loc> {
        /// Create a Substitution Patterns Location at the zero-point,
        /// for a substitution depth as specified and dimensions of the input and
        /// output data as specified by the type.
        ///
        /// This panic!s on dynamic sized location types nya
        pub fn zeroed(substitution_depth: usize) -> Self {
            Self {
                current_location: Array2::zeros(Ix2(
                    substitution_depth + 1, 
                    Loc::NDIM.expect(
                        "zero function only works on static sized index types"
                    )
                )),
                subdepth: substitution_depth,
                dimensions: Loc::NDIM.unwrap(),
                _a: Default::default()
            }
        }

        /// Increment an axis in the context of the input data and patterns
        /// being the given sizes nya
        ///
        /// If there is overflow we return that ^.^
        ///
        /// Probably panics on invalid axis.
        #[inline]
        pub fn increment_axis<InSize: Dimension, PatSize: Dimension>(
            &mut self, axis: Axis, input_size: &InSize, pattern_size: &PatternSize::<PatSize> 
        ) -> AxisIncrStatus {
            let patsize_in_axis = pattern_size.raw_dim()[axis.0];
            let insize_in_axis = input_size[axis.0];
            // view over the location in the given axis nya
            let mut axis_view = self.current_location.index_axis_mut(
                Axis(1),
                axis.0  // The index *is* the axis since the data is a location nya
            );

            for (subdepth_zero_is_input, current_moddigit) in axis_view
                .iter_mut().enumerate().rev() {
                match subdepth_zero_is_input {
                    // this is for the actual input - we match on the input size
                    0 => match *current_moddigit {
                        n if n == insize_in_axis - 1 => {
                            // overflowed so keep going
                            *current_moddigit = 0;
                        }
                        n => {
                            *current_moddigit = n + 1;
                            return AxisIncrStatus::Fine;
                        }
                    }
                    // otherwise use the pattern size nya
                    _ => match *current_moddigit {
                        n if n == patsize_in_axis - 1 => {
                            *current_moddigit = 0;
                        }
                        n => {
                            *current_moddigit = n + 1;
                            return AxisIncrStatus::Fine;
                        }
                    }
                }
            };
            // if we haven't gotten out as fine beforehand nya
            // this means we got to the end and in doing so overflowed nya
            return AxisIncrStatus::Overflow;
        }

        pub fn increment <
            InSize: Dimension, 
            PatSize: Dimension,
            MemoryOrder: GenMemoryOrder
        > (
            &mut self, 
            input_size: &InSize, 
            pattern_size: &PatternSize::<PatSize>,
            memory_order: &MemoryOrder
        ) -> PatLocIncrStatus {
            let mut maybe_curr_axis = NextAxisIncr::NextAxis(
                memory_order.first_axis()
            );

            while let NextAxisIncr::NextAxis(curr_axis) = maybe_curr_axis {
                let curr_axis_incr_status = self.increment_axis(
                    curr_axis, 
                    input_size,
                    pattern_size
                );

                match curr_axis_incr_status {
                    AxisIncrStatus::Fine => return PatLocIncrStatus::Fine,
                    AxisIncrStatus::Overflow => ()
                }

                maybe_curr_axis = memory_order.next_axis(curr_axis);
            };
        
            PatLocIncrStatus::NoMoreLocations
        }

        /// The coordinate in the pattern or input at the given 
        /// substitution depth (0 is 0 substitutions, so is the coordinate in
        /// the input nya)
        pub fn subcoordinate_at_subdepth(&self, subdepth: usize) 
            -> ArrayView1::<'_, usize> {
            self.current_location.index_axis(Axis(0), subdepth)
        }
    }

    impl <
        'inarr, 
        'insubs, 
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > PatternGenerator::<
        'inarr, 
        'insubs, 
        SubDim, 
        InSize, 
        MemoryOrder
    > {
        pub fn get_for_loc(&self, index: &SubPatsLocation::<SubDim>) -> InternalState {
            let input_arr = &self.input;
            // 0 is taking from the input.
            // 1..substitution_depth + 1 is the levels of substitution depth
            let mut current_substitution = 0usize;
            
            // Get the input/pattern coordinate for the given substitution depth nya
            //
            // Panics on invalid ndim in &SubPatsLocation 
            fn get_dim<PatDim: Dimension> (
                index: &SubPatsLocation::<PatDim>, 
                subdepth: usize
            ) -> PatDim {
                let subcoord_at_subdepth = index.subcoordinate_at_subdepth(subdepth);
                // Static sized ones get free optimisation! nya
                let mut idx = PatDim::zeros(PatDim::NDIM.unwrap_or(index.dimensions));
                idx.slice_mut().iter_mut()
                    .zip(subcoord_at_subdepth.iter())
                    .for_each(|(out_idx, in_idx)| *out_idx = *in_idx);
                idx
            }

            let mut current_state: InternalState = input_arr[
                InSize::from_dimension(&get_dim(index, current_substitution))
                .expect("Someone borked up dimensions nya")
            ];
            // note that this is explicitly an inclusive range nya => 
            // += inside loop means we include the current location nya
            // [0] => input array coordinates
            // [1..=substitution_depth] is the pattern coordinates.
            while current_substitution < self.substitution_depth {
                current_substitution += 1;
                let dim = get_dim(index, current_substitution);
                let dim_for_sub = 
                    <<SubDim::Larger as Dimension>::Smaller as Dimension>::from_dimension(
                    &dim
                ).expect("panic! here indicates borked dimensions again >.< nyaa");
                current_state = self.substitutions.subs_for(current_state)[dim_for_sub];
            }
            current_state
        }

        /// Create the first location in this pattern generator nya
        pub fn create_first_location(&self) -> SubPatsLocation::<SubDim> {
            SubPatsLocation::<SubDim>::zeroed(self.substitution_depth)
        }
    }

    #[derive(Clone, Debug)]
    enum PatGenIterState <SubDim: Dimension> {
        Itering {
            currloc: SubPatsLocation::<SubDim>,
            num_iterations_done: usize
        },
        Done
    }

    /// An iterator over the coordinates for a pattern generator
    pub struct PatGenIterator<
        'inarr, 
        'insubs,
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > {
        gen: PatternGenerator::<'inarr, 'insubs, SubDim, InSize, MemoryOrder>,
        state: PatGenIterState::<SubDim>
    }

    impl<
        'inarr, 
        'insubs,
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > PatGenIterator::<'inarr, 'insubs, SubDim, InSize, MemoryOrder> {
        pub fn pattern_generator(&self) -> &PatternGenerator<
            'inarr, 'insubs, SubDim, InSize, MemoryOrder
        > {
            &self.gen
        }
    }

    impl <
        'inarr, 
        'insubs, 
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > IntoIterator for PatternGenerator::<
        'inarr, 'insubs, SubDim, InSize, MemoryOrder
    > {
        type Item = InternalState;

        type IntoIter = PatGenIterator<'inarr, 'insubs, SubDim, InSize, MemoryOrder>;

        fn into_iter(self) -> Self::IntoIter {
            let first_loc = self.create_first_location();
            PatGenIterator {
                gen: self,
                state: PatGenIterState::Itering {
                    currloc: first_loc,
                    num_iterations_done: 0
                }
            }
        }
    }

    impl <
        'inarr, 
        'insubs, 
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > Iterator for PatGenIterator::<
        'inarr, 'insubs, SubDim, InSize, MemoryOrder
    > {
        type Item = InternalState;

        fn next(&mut self) -> Option<Self::Item> {
            match &mut self.state {
                PatGenIterState::Itering {currloc, num_iterations_done} => {
                    // Get current location (this is what is spat out nya)
                    let state = self.gen.get_for_loc(&currloc);
                    // do incrementing ^.^
                    match currloc.increment(
                        &self.gen.input.raw_dim(),
                        &self.gen.pattern_size,
                        &self.gen.generation_order
                    ) {
                        PatLocIncrStatus::Fine => {
                            *num_iterations_done += 1;
                            Some(state)
                        }
                        PatLocIncrStatus::NoMoreLocations => {
                            self.state = PatGenIterState::Done;
                            Some(state)
                        }
                    }
                }
                PatGenIterState::Done => {None}
            }
        }
        // (lower, upper bound)
        fn size_hint(&self) -> (usize, Option<usize>) {
            let total_elems = self.pattern_generator().total_output_size;
            let remaining_elems = match self.state {
                PatGenIterState::Itering {num_iterations_done: n, ..} => total_elems - n,
                PatGenIterState::Done => 0
            };
            (remaining_elems, Some(remaining_elems))
        }
    } 

    impl <
        'inarr, 
        'insubs, 
        SubDim: Dimension, 
        InSize: Dimension, 
        MemoryOrder: GenMemoryOrder
    > ExactSizeIterator for PatGenIterator::<
        'inarr, 'insubs, SubDim, InSize, MemoryOrder
    > {}

}



/*
patsubgen - multidimensional pattern substitution
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
