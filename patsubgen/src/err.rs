///! Holds errors for this library

use crate::internal_state;
use crate::ndarray::Dimension;
use std::error::Error;
use std::fmt::Display;
use crate::display::*;



/// Holds an error created by trying to access a substitution with an invalid internal
/// state nya
///
/// The sequential completeness of internal states means that the internal state count
/// provides (or should provide) all the available state (0 to state count - 1)
#[derive(Copy, Clone, Debug)]
pub struct SubstitutionAccessErr {
    /// The passed invalid internal state for substitution nya
    pub invalid_state: internal_state::InternalState,
    /// The available states for substitution.
    pub available_states: internal_state::InternalStateCount
}

impl SubstitutionAccessErr {
    /// Create an error for accessing a substitution with an incorrect state nya
    pub fn new(invalid_state: internal_state::InternalState, available_states: internal_state::InternalStateCount) -> Self {
        Self {
            invalid_state,
            available_states
        }
    }
}

impl Display for SubstitutionAccessErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f, 
            concat!(
                "Substituiton Error (found state): invalid_state:", 
                "{}; with available_states: {};"
            ), 
            self.invalid_state.0, 
            self.available_states.0
        )
    }

}


impl Error for SubstitutionAccessErr {}



/// Represents any error involving the size of a pattern - 
/// either axis size error or dimensionality (# dimensions) error
/// nyaa
#[derive(Clone, Debug)]
pub enum InternalPatternSizeErr <DimSizeExpected: Dimension, DimSizeProvided: Dimension> {
    Dimensionality(PatternNDimErr),
    AxisSize(PatternAxisSizeErr::<DimSizeExpected, DimSizeProvided>)

}

impl <DimSizeExp: Dimension, DimSizeProv: Dimension> From <PatternNDimErr> for InternalPatternSizeErr::<DimSizeExp, DimSizeProv> {
    fn from(data: PatternNDimErr) -> Self {
        Self::Dimensionality(data)
    }
}

impl <DimSizeExp: Dimension, DimSizeProv: Dimension> From <
        PatternAxisSizeErr::<DimSizeExp, DimSizeProv>
    > for InternalPatternSizeErr::<DimSizeExp, DimSizeProv> {
    fn from(data: PatternAxisSizeErr::<DimSizeExp, DimSizeProv>) -> Self {
        Self::AxisSize(data)
    } 
}

impl <DSE: Dimension, DSP: Dimension> Display for InternalPatternSizeErr::<DSE, DSP> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Dimensionality(err) => {
                err.fmt(f)
            }
            Self::AxisSize(err) => {
                err.fmt(f)
            }
        }
    }
    
}


/// Represents an error based off mismatched sizes nya
#[derive(Clone, Debug)]
pub struct PatternAxisSizeErr <DimSizeExpected: Dimension, DimSizeProvided: Dimension> {
    size_expected: internal_state::PatternSize::<DimSizeExpected>,
    size_provided: internal_state::PatternSize::<DimSizeProvided>
}

impl <DSE: Dimension, DSP: Dimension> PatternAxisSizeErr <DSE, DSP>{
    /// Create an error based off if the two pattern sizes don't match in values nya
    /// The type itself does the checking :)
    pub fn maybe_err(
        expected: &internal_state::PatternSize::<DSE>, 
        provided: &internal_state::PatternSize::<DSP>
    ) -> Result<(), InternalPatternSizeErr::<DSE, DSP>> {
        /*
        match InternalPatternDimensionalityErr::from_pattern_sizes(expected, provided) {
            Ok(_) => {},
            Err(dimerr) => return Err(InternalPatternSizeErr::Dimensionality(dimerr))
        }
        */

        PatternNDimErr::from_pattern_sizes_maybe_err(expected, provided)?;

        if *expected == *provided {return Ok(())};
        Err(InternalPatternSizeErr::AxisSize(Self {
            size_expected: expected.clone(),
            size_provided: provided.clone() 
        }))
    }
}

impl <DSE: Dimension, DSP: Dimension> std::fmt::Display for PatternAxisSizeErr <DSE, DSP> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            concat!(
                "Expected pattern size: {};",
                "Got pattern size: {} nya"
            ),
            DisplayableDimension(self.size_expected.raw_dim()),
            DisplayableDimension(self.size_provided.raw_dim())
        )
    }
}

impl <DSE: Dimension, DSP: Dimension> Error for PatternAxisSizeErr<DSE, DSP> {}



/// An error based off of mismatched dimensions.
#[derive(Clone, Debug)]
pub struct PatternNDimErr {
    expected_ndim: usize,
    provided_ndim: usize
}

impl PatternNDimErr {
    /// Create a new dimensionality error if there actually is one
    /// nya
    pub fn maybe_err(expected_ndim: usize, provided_ndim: usize) -> Result<(), Self> {
        if expected_ndim != provided_ndim {
            Err(Self{expected_ndim, provided_ndim})
        } else {
            Ok(())
        }
    }
    
    /// Check the sizes on patterns nyaa
    pub fn from_pattern_sizes_maybe_err <
        DimSizeExpected: Dimension, 
        DimSizeProvided: Dimension
    >(
        expected_size_dims: &internal_state::PatternSize::<DimSizeExpected>,
        provided_size_dims: &internal_state::PatternSize::<DimSizeProvided>
    ) -> Result<(), Self> {
        Self::maybe_err(expected_size_dims.ndim(), provided_size_dims.ndim())
    }
}

impl Display for PatternNDimErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f, 
            "Expected {}-dimensional pattern, got {}-dimensional pattern",
            self.expected_ndim, self.provided_ndim
        )
    }
}

impl Error for PatternNDimErr {}

/// Error for attempting to access an internal state substittution and replace/modify it nya
#[derive(Clone, Debug)]
pub enum InternalPatternAccessModificationErr <Dim: Dimension> {
    Size(InternalPatternSizeErr::<Dim, Dim>),
    SubAccess(SubstitutionAccessErr)
}

impl <Dim: Dimension> From <InternalPatternSizeErr::<Dim, Dim>> for InternalPatternAccessModificationErr::<Dim> {
    fn from(data: InternalPatternSizeErr::<Dim, Dim>) -> Self {
        Self::Size(data)
    } 
}

impl <Dim: Dimension> From <SubstitutionAccessErr> for InternalPatternAccessModificationErr::<Dim> {
    fn from(data: SubstitutionAccessErr) -> Self {
        Self::SubAccess(data)
    }
    
}


// {{{ Errors for validating a pattern substitution is self-consistent nya

#[derive(Clone)]
/// Error with internal consistency of 
pub enum InternalConsistencyErr <ISPS: internal_state::PatternSubstitution> {
    /// The substitution contains discontinuous states not matching those available in
    /// (0..state_count)
    InputStateDiscontinuity {
        subs: ISPS,
        discontinuity: internal_state::MissingInputStates
    },
    /// Transform is open on the set of states - there are states without
    /// presence in the input states nyaa
    InputStateOpenTransform {
        subs: ISPS,
        excess_states: internal_state::SubsExcessStates
    }
}

impl <ISPS: internal_state::PatternSubstitution> std::fmt::Debug 
for InternalConsistencyErr::<ISPS> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as Display>::fmt(self, f)
    }
}

impl <ISPS: internal_state::PatternSubstitution> Display for InternalConsistencyErr::<ISPS> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InputStateDiscontinuity {subs: _, discontinuity} => {
                write!(f, "Discontinuity: {:?}", discontinuity) 
            },
            Self::InputStateOpenTransform {subs: _, excess_states} => {
                write!(f, "Non-substituted states: {:?}", excess_states)
            }
        }
    }
}

impl <ISPS: internal_state::PatternSubstitution> Error for InternalConsistencyErr::<ISPS> {}

// }}}



/// Error for dimensionality of actual substitution generators not matching nyaa
#[derive(Clone, Copy, Debug)]
pub struct InputNDimErr  {
    /// the dimensionality of the array for which substitutions should be performed
    /// nya
    array_input_dimensionality: usize,
    /// The dimensionality of the substitutions/patterns 
    substituted_pattern_dimensionality: usize
}

impl InputNDimErr {
    /// Maybe create a mismatched dimensionality error if the dimensionalities
    /// are wrong nya
    ///
    /// used like ::new_err(...)?;
    pub fn new_err<DimInArr: Dimension, DimSubstitution: Dimension>(
        in_array_dim: &DimInArr, 
        substitution_dim: &DimSubstitution 
    ) -> Result::<(), Self> {
        if in_array_dim.ndim() == substitution_dim.ndim() {
            Ok(())
        } else {
            Err(Self{
                array_input_dimensionality: in_array_dim.ndim(),
                substituted_pattern_dimensionality: substitution_dim.ndim()
            })
        }
    }
}

impl std::fmt::Display for InputNDimErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { 
        write!(
            f, 
            "subbed array dimensionality: {}, sub dimensionality: {}, non-matching.",
            self.array_input_dimensionality,
            self.substituted_pattern_dimensionality
        )
    }
}

impl Error for InputNDimErr {}

/*
patsubgen - multidimensional pattern substitution
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
