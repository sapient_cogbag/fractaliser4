///! Some simple utility/nutypes for displaying things nya
use crate::ndarray::Dimension;
use std::fmt::Display;

/// Simply holds a dimension and makes it displayable ;p
pub struct DisplayableDimension <'a, Dim: Dimension> (pub &'a Dim);


impl <'a, Dim: Dimension> Display for DisplayableDimension <'a, Dim> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[")?;
        match self.0.ndim() {
            0 => {
                write!(f, "]")
            },
            nonzero_dim => {
                let mut dims_iter = self.0.slice().iter();
                for dimension_size in (&mut dims_iter).take(nonzero_dim - 1) {
                    write!(f, "{}, ", dimension_size)?;
                }
                // take the last chunk of the iterator.
                // There should always be something here since we checked 
                // the size nyaaa
                let last = dims_iter.next();
                match last {
                    Some(axis_size) => {
                        write!(f, "{}]", axis_size)
                    }
                    None => panic!()
                }
            }
        }
    } 
}



/*
patsubgen - multidimensional pattern substitution
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
