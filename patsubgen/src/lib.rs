//! Module for generating substitions in arbitrary dimensions in
//! a lazy manner nya. Patterns have fixed size.
//! 
//! This problem reduces to that of representing a number in base <pattern-size>
//! on each side and incrementing that nya

pub mod state_translation;
pub mod user_state;
pub mod internal_state;
pub mod err;
pub mod substitution;
pub mod display;

pub use ndarray::Dimension;
pub use ndarray::prelude::*;
pub use ndarray::prelude;
pub use ndarray;
pub use internal_state::{InternalState, InternalStateCount};




#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}


/*
patsubgen - multidimensional pattern substitution
Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
